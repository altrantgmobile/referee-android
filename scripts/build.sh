#!/bin/bash
# Accept Licenses
mkdir "${ANDROID_HOME}/licenses" || true
echo "8933bad161af4178b1185d1a37fbf41ea5269c55" > "${ANDROID_HOME}/licenses/android-sdk-license"
echo "84831b9409646a918e30573bab4c9c91346d8abd" > "${ANDROID_HOME}/licenses/android-sdk-preview-license"

# Build
if ./gradlew clean testDebugUnitTest copyTestClasses jacocoTestDebugUnitTestReport; then
  echo "Build SUCCESS" >&2
  bash <(curl -s https://codecov.io/bash) -t 844fac92-1a1f-4dc7-891d-7a0030e295c7
  ./gradlew versionEyeUpdate
else
  echo "Build FAILED" >&2
fi
