
[![codecov](https://codecov.io/bb/altrantgmobile/referee-android/branch/master/graph/badge.svg)](https://codecov.io/bb/altrantgmobile/referee-android)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/b63c0ecca4694802b06c8447cf3bdf17)](https://www.codacy.com/app/entreco/referee-android?utm_source=altrantgmobile@bitbucket.org&amp;utm_medium=referral&amp;utm_content=altrantgmobile/referee-android&amp;utm_campaign=Badge_Grade)
[![Dependency Status](https://www.versioneye.com/user/projects/5994a0646725bd004bd84216/badge.svg?style=flat-square)](https://www.versioneye.com/user/projects/5994a0646725bd004bd84216)
[![Awesome Kotlin Badge](https://kotlin.link/awesome-kotlin.svg)](https://github.com/KotlinBy/awesome-kotlin)

referee-android
===============