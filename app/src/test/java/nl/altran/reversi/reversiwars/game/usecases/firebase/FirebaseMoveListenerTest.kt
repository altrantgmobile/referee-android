package nl.altran.reversi.reversiwars.game.usecases.firebase

import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import nl.altran.reversi.data.Move
import org.junit.Assert
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class FirebaseMoveListenerTest {

    @Mock private lateinit var unregister: (FirebaseMoveListener) -> Unit
    @Mock private lateinit var complete: (Move) -> Unit
    @InjectMocks lateinit var subject: FirebaseMoveListener

    @Test
    fun `it should not invoke lambda's when json == ""`() {
        subject.onDataChange("")

        verifyZeroInteractions(complete, unregister)
    }

    @Test
    fun `it should not invoke lambda's when json == invalid`() {
        subject.onDataChange(":{..aa ]")

        verifyZeroInteractions(complete, unregister)
    }

    @Ignore("somehow called twice according to Mockito")
    @Test
    fun `it should invoke 'complete' lambda when json == valid`() {
        val moveCaptor = argumentCaptor<Move>()

        subject.onDataChange("{\"row\":1, \"col\":2}")

        verify(complete).invoke(moveCaptor.capture())

        Assert.assertEquals(1, moveCaptor.firstValue.row)
        Assert.assertEquals(2, moveCaptor.firstValue.col)
    }

    @Ignore("somehow not invoked according to Mockito")
    @Test
    fun `it should invoke 'unregister' lambda when json == valid`() {
        subject.onDataChange("{\"row\":1, \"col\":2}")

        verify(unregister).invoke(subject)
    }
}