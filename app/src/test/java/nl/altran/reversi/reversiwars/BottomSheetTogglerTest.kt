package nl.altran.reversi.reversiwars

import android.support.design.widget.BottomSheetBehavior
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class BottomSheetTogglerTest {

    class TestToggler(private val runnable: Runnable) : BottomSheetToggler() {
        override fun onToggleAndSlide(index: Int, perc: Float) {
            runnable.run()
        }
    }

    @InjectMocks lateinit var subject: TestToggler
    @Mock private lateinit var mockRunnable: Runnable

    @Test
    fun `it should call onToggle`() {
        subject.onStateChanged(mock(), BottomSheetBehavior.STATE_EXPANDED)
        verify(mockRunnable).run()
    }

    @Test
    fun `it should call onToggle only once`() {
        subject.onStateChanged(mock(), BottomSheetBehavior.STATE_EXPANDED)
        subject.onStateChanged(mock(), BottomSheetBehavior.STATE_EXPANDED)
        verify(mockRunnable, times(1)).run()
    }

    @Test
    fun `it should call onToggle every time`() {
        subject.onStateChanged(mock(), BottomSheetBehavior.STATE_EXPANDED)
        subject.onStateChanged(mock(), BottomSheetBehavior.STATE_EXPANDED)
        subject.onStateChanged(mock(), BottomSheetBehavior.STATE_COLLAPSED)
        subject.onStateChanged(mock(), BottomSheetBehavior.STATE_COLLAPSED)
        verify(mockRunnable, times(2)).run()
    }
}