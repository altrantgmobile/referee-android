package nl.altran.reversi.reversiwars.model

import android.databinding.ObservableField
import org.junit.Assert.assertEquals
import org.junit.Test

class StoneTest {

    @Test
    fun `it should be EMPTY by default`() {
        assertEquals(Stone.Color.EMPTY, Stone(0, 1).color.get())
    }

    @Test
    fun `it should not flip EMPTY`() {
        assertEquals(Stone.Color.EMPTY, Stone(0, 1).flip().color.get())
    }

    @Test
    fun `it should flip BLACK to WHITE`() {
        assertEquals(Stone.Color.WHITE, black().flip().color.get())
    }

    @Test
    fun `it should flip WHITE to BLACK`() {
        assertEquals(Stone.Color.BLACK, white().flip().color.get())
    }

    @Test
    fun `it should flip StoneColorWHITE correct`() {
        assertEquals(Stone.Color.BLACK, Stone.Color.WHITE.flip())
    }

    @Test
    fun `it should flip StoneColorBLACK correct`() {
        assertEquals(Stone.Color.WHITE, Stone.Color.BLACK.flip())
    }

    @Test
    fun `it should flip StoneColorEMPTY correct`() {
        assertEquals(Stone.Color.EMPTY, Stone.Color.EMPTY.flip())
    }

    private fun black() = Stone(0, 1, ObservableField(Stone.Color.BLACK))
    private fun white() = Stone(0, 1, ObservableField(Stone.Color.WHITE))
}