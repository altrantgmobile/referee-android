package nl.altran.reversi.reversiwars.model.util

import com.google.gson.JsonSyntaxException
import nl.altran.reversi.reversiwars.model.board.Board
import nl.altran.reversi.reversiwars.model.Stone
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class BoardUtilTest {

    @Test(expected = IllegalArgumentException::class)
    fun `it should throw when board is empty`() {
        Assert.assertNull(BoardUtil.extractBoard(""))
    }

    @Test(expected = JsonSyntaxException::class)
    fun `it should throw when board is invalid`() {
        Assert.assertNull(BoardUtil.extractBoard("invalid"))
    }

    @Test
    fun `it should parse board correctly`() {
        val board = Board()
        Assert.assertNotNull(BoardUtil.extractBoard(board.toJson()))
    }

    @Test
    fun `it should recreate initial board correctly`() {
        val board = Board()
        val recreated = BoardUtil.extractBoard(board.toJson())
        Assert.assertEquals(board.toJson(), recreated.toJson())
    }

    @Test
    fun `it should recreate board (0,7, White) correctly`() {
        val board = Board()
        board.set(Stone(0, 7), Stone.Color.WHITE)

        val recreated = BoardUtil.extractBoard(board.toJson())
        Assert.assertEquals(board.toJson(), recreated.toJson())
    }

    @Test
    fun `it should recreate board (0,7, Black) correctly`() {
        val board = Board()
        board.set(Stone(0, 7), Stone.Color.BLACK)

        val recreated = BoardUtil.extractBoard(board.toJson())
        Assert.assertEquals(board.toJson(), recreated.toJson())
    }

    @Test
    fun `it should recreate board (7,7, White) correctly`() {
        val board = Board()
        board.set(Stone(7, 7), Stone.Color.WHITE)

        val recreated = BoardUtil.extractBoard(board.toJson())
        Assert.assertEquals(board.toJson(), recreated.toJson())
    }

    @Test
    fun `it should recreate board (7,7, Black) correctly`() {
        val board = Board()
        board.set(Stone(7, 7), Stone.Color.BLACK)

        val recreated = BoardUtil.extractBoard(board.toJson())
        Assert.assertEquals(board.toJson(), recreated.toJson())
    }
}