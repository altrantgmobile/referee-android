package nl.altran.reversi.reversiwars.select_players.usecases.firebase

import nl.altran.reversi.reversiwars.game.usecases.PlayGameUsecase
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class FirebasePlayerMapperTest {

    private val uuid = "uuid"

    @Mock private lateinit var mockUsecase: PlayGameUsecase
    @InjectMocks lateinit var subject: FirebasePlayerMapper

    @Test
    fun `it should return null when json syntax is invalid`() {
        Assert.assertNull(subject.toRemote("!!{ '\"haha", uuid, mockUsecase))
    }

    @Test
    fun `it should return null when josn is empty`() {
        Assert.assertNull(subject.toRemote("", uuid, mockUsecase))
    }

    @Test
    fun `it should return non-null PlayerData when json is valid`() {
        Assert.assertNotNull(subject.toRemote("{\"public\":{\"name\":\"jacoco\"}}", uuid, mockUsecase))
    }

    @Test
    fun `it should return correct player name() when json is valid`() {
        val remote = subject.toRemote("{\"public\":{\"name\":\"jacoco\"}}", uuid, mockUsecase)
        Assert.assertEquals("jacoco", remote!!.name())
    }
}