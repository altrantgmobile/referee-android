package nl.altran.reversi.reversiwars.model

import nl.altran.reversi.reversiwars.model.players.BasePlayer

class TestHumanPlayer : BasePlayer() {

    override fun name(): String {
        return "testPlayer"
    }

    override fun human(): Boolean {
        return true
    }

    override fun local(): Boolean {
        return true
    }

    override fun onYourTurn(board: String) {
        // noop
    }

    override fun onRejected(board: String) {
        // noop
    }
}