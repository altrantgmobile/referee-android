package nl.altran.reversi.reversiwars.model.players

import com.nhaarman.mockito_kotlin.verify
import nl.altran.reversi.reversiwars.game.usecases.PlayGameUsecase
import nl.altran.reversi.reversiwars.model.Stone
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class RemotePlayerTest {

    companion object {
        val gameUuid = UUID.randomUUID().toString()
    }

    @Mock private lateinit var mockRemoteRepo: PlayGameUsecase
    lateinit var subject: RemotePlayer

    @Before
    fun setUp() {
        subject = RemotePlayer("remote", mockRemoteRepo, "1")
        subject.stoneColor = Stone.Color.BLACK
    }

    @Test
    fun `it should create a match when onJoinedGameIsCalled`() {
        subject.onJoinedGame(gameUuid)

        verify(mockRemoteRepo).joinGame(gameUuid, Stone.Color.BLACK)
    }

    @Test
    fun `it should store WIN when you won the game`() {
        subject.onGameFinished(64, 0)
        verify(mockRemoteRepo).gameFinished(64, 0)
    }

    @Test
    fun `it should store DRAW when game ends in a tie`() {
        subject.onGameFinished(1, 1)
        verify(mockRemoteRepo).gameFinished(1, 1)
    }

    @Test
    fun `it should store LOSS when you loose the game`() {
        subject.onGameFinished(0, 64)
        verify(mockRemoteRepo).gameFinished(0, 64)
    }
}