package nl.altran.reversi.reversiwars.model.players

import org.junit.Assert.*
import org.junit.Test

class RandomPlayerTest {

    @Test
    fun `name() should return ctor parameter`() {
        assertEquals("Random", RandomPlayer().name())
    }

    @Test
    fun `human() should return false`() {
        assertFalse(RandomPlayer().human())
    }

    @Test
    fun `local() should return true`() {
        assertTrue(RandomPlayer().local())
    }
}