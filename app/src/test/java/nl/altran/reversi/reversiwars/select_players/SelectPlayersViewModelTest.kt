package nl.altran.reversi.reversiwars.select_players

import android.arch.lifecycle.Lifecycle
import com.nhaarman.mockito_kotlin.verify
import nl.altran.reversi.reversiwars.model.players.HumanPlayer
import nl.altran.reversi.reversiwars.select_players.usecases.CreateMatchUsecase
import nl.altran.reversi.reversiwars.select_players.usecases.FetchPlayersUsecase
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SelectPlayersViewModelTest{

    @Mock private lateinit var lifecycle: Lifecycle
    @Mock private lateinit var fetchPlayersUsecase: FetchPlayersUsecase
    @Mock private lateinit var createMatchUsecase: CreateMatchUsecase

    @InjectMocks lateinit var subject: SelectPlayersViewModel

    private val player = HumanPlayer("hoi")

    @Test
    fun `it should start observing lifecycle in init{}`() {
        verify(lifecycle).addObserver(subject)
    }

    @Test
    fun `it should listen for players on start()`() {
        subject.onStart()

        verify(fetchPlayersUsecase).registerCallback(subject)
    }

    @Test
    fun `it should stop listening for players on stop()`() {
        subject.onStop()

        verify(fetchPlayersUsecase).unregisterCallback()
    }
    @Test
    fun `it should stop observing lifecycle on stop()`() {
        subject.onStop()

        verify(lifecycle).removeObserver(subject)
    }

    @Test
    fun `it should add players when retrieved`() {
        subject.onPlayerRetrieved(player)

        Assert.assertTrue(subject.players.contains(player))
    }

    @Test
    fun `it should remove players when remove`() {
        subject.onPlayerRetrieved(player)
        subject.onPlayerRemoved(player.name())

        Assert.assertFalse(subject.players.contains(player))
    }

    @Test
    fun `it should create new match if 2 players are selected`() {
        subject.onPlayerSelected(player)
        subject.onPlayerSelected(player)

        verify(createMatchUsecase).go(player, player)
    }
}