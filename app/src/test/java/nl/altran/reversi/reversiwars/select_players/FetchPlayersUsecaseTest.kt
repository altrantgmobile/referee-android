package nl.altran.reversi.reversiwars.select_players

import com.nhaarman.mockito_kotlin.verify
import nl.altran.reversi.reversiwars.select_players.usecases.FetchPlayersUsecase
import nl.altran.reversi.reversiwars.select_players.usecases.PlayersRepository
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class FetchPlayersUsecaseTest{

    @InjectMocks lateinit var subject: FetchPlayersUsecase

    @Mock private lateinit var mockRepo: PlayersRepository
    @Mock private lateinit var mockCallback: FetchPlayersUsecase.Callback

    @Test
    fun `it should fetch remote player`() {
        subject.registerCallback(mockCallback)

        verify(mockRepo).fetchRemotePlayers(mockCallback)
    }
}