package nl.altran.reversi.reversiwars.model.players

import nl.altran.reversi.reversiwars.model.Stone
import nl.altran.reversi.reversiwars.model.board.Board
import nl.altran.reversi.reversiwars.model.util.BoardUtil
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TamenoriTest {

    val subject = Tamenori(2)

    @Test
    fun `it should find possible moves for WHITE on initial board`() {
        val moves = BoardUtil.findPossibleMoves(Board(), Stone.Color.WHITE)
        assertEquals(4, moves.size)
        assertEquals(2, moves[0].row)
        assertEquals(4, moves[0].col)
    }

    @Test
    fun `it should find possible moves for BLACK on initial board`() {
        val moves = BoardUtil.findPossibleMoves(Board(), Stone.Color.BLACK)
        assertEquals(4, moves.size)
    }

    @Test
    fun `it should find possible moves for WHITE correct for early board`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_early.json")
        val moves = BoardUtil.findPossibleMoves(board, Stone.Color.WHITE)
        assertEquals(4, moves.size)
    }

    @Test
    fun `it should find possible moves for BLACK correct for early board`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_early.json")
        val moves = BoardUtil.findPossibleMoves(board, Stone.Color.BLACK)
        assertEquals(4, moves.size)
    }

    @Test
    fun `it should evalCorners() correct for initial board`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_start.json")
        assertEquals(0, subject.evalCorners(board))
    }

    @Test
    fun `it should evalCorners() correct for early board`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_early.json")
        assertEquals(3, subject.evalCorners(board))
    }

    @Test
    fun `it should evalScore() correct for initial board`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_start.json")
        assertEquals(0, subject.evalScore(board))
    }

    @Test
    fun `it should evalScore() correct for early board`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_early.json")
        assertEquals(12, subject.evalScore(board))
    }

    @Test
    fun `it should evalSafeStones() correct for initial board`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_start.json")
        assertEquals(0, subject.evalSafeStones(board))
    }

    @Test
    fun `it should evalSafeStones() correct 1`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_safe_1.json")
        assertEquals(1, subject.evalSafeStones(board))
    }

    @Test
    fun `it should evalSafeStones() correct 2`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_safe_2.json")
        assertEquals(2, subject.evalSafeStones(board))
    }

    @Test
    fun `it should evalSafeStones() correct 3`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_safe_3.json")
        assertEquals(2, subject.evalSafeStones(board))
    }

    @Test
    fun `it should evalSafeStones() correct 4`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_safe_4.json")
        assertEquals(10, subject.evalSafeStones(board))
    }

    @Test
    fun `it should evalSafeStones() correct 5`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_safe_5.json")
        assertEquals(5, subject.evalSafeStones(board))
    }

    @Test
    fun `it should evalSafeStones() correct 6`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_safe_6.json")
        assertEquals(-5, subject.evalSafeStones(board))
    }

    @Test
    fun `it should evalSafeStones() correct 7`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_safe_7.json")
        assertEquals(-5, subject.evalSafeStones(board))
    }

    @Test
    fun `it should evalSafeStones() correct 8`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_safe_8.json")
        assertEquals(16, subject.evalSafeStones(board))
    }

    @Test
    fun `it should evalSafeStones() correct 9`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_safe_9.json")
        assertEquals(-5, subject.evalSafeStones(board))
    }

    @Test
    fun `it should evalSafeStones() correct 10`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_safe_10.json")
        assertEquals(0, subject.evalSafeStones(board))
    }

    @Test
    fun `it should evalSafeStones() correct 10_horiz`() {
        val board = BoardUtil.extractBoardFromFile("eval_board_safe_10_horiz.json")
        assertEquals(0, subject.evalSafeStones(board))
    }
}