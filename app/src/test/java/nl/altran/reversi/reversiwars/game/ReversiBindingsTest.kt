package nl.altran.reversi.reversiwars.game

import android.content.Context
import android.view.ViewPropertyAnimator
import android.widget.ImageView
import com.nhaarman.mockito_kotlin.*
import nl.altran.reversi.reversiwars.game.ui.ReversiBindings
import nl.altran.reversi.reversiwars.model.Stone
import nl.altran.reversi.reversiwars.model.players.HumanPlayer
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations.initMocks
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.verification.VerificationMode

@RunWith(MockitoJUnitRunner::class)
class ReversiBindingsTest {

    @Mock private lateinit var mockView: ImageView
    @Mock private lateinit var mockContext: Context
    @Mock private lateinit var mockViewPropertyAnimator: ViewPropertyAnimator

    private val me = HumanPlayer()
    private val opponent = HumanPlayer()

    @Before
    fun setUp() {
        initMocks(this)

        whenever(mockView.context).thenReturn(mockContext)
        whenever(mockView.animate()).thenReturn(mockViewPropertyAnimator)
        whenever(mockViewPropertyAnimator.scaleX(any())).thenReturn(mockViewPropertyAnimator)
        whenever(mockViewPropertyAnimator.scaleY(any())).thenReturn(mockViewPropertyAnimator)
        whenever(mockViewPropertyAnimator.alpha(any())).thenReturn(mockViewPropertyAnimator)
    }

    @Test
    fun `it should animate to default initially`() {
        ReversiBindings.doCurrentPlayerAnimation(mockView, null, null)
        verifyAnimateTo(scale = 1F, alpha = 1F)
    }

    @Test
    fun `it should shrink when opponent's turn`() {
        ReversiBindings.doCurrentPlayerAnimation(mockView, me = me, current = opponent)
        verifyAnimateTo(scale = .5F, alpha = .3F)
    }

    @Test
    fun `it should grow when my turn`() {
        ReversiBindings.doCurrentPlayerAnimation(mockView, me = me, current = me)
        verifyAnimateTo(scale = 1F, alpha = 1F)
    }

    @Test
    fun `it should animate to default if current player = null`() {
        ReversiBindings.doCurrentPlayerAnimation(mockView, me = me, current = null)
        verifyAnimateTo(scale = 1F, alpha = 1F)
    }

    @Test
    fun `it should animate to default if me == null`() {
        ReversiBindings.doCurrentPlayerAnimation(mockView, me = null, current = me)
        verifyAnimateTo(scale = 1F, alpha = 1F)
    }

    @Test
    fun `it should animate white reject() if reject === me && me === white`() {
        me.stoneColor(Stone.Color.WHITE)

        ReversiBindings.doRejectWhiteAnimation(mockView, reject = me, stamp = System.currentTimeMillis())

        verifyShakeAnimation(times(1))
    }

    @Test
    fun `it should not animate white reject() if reject === me && me != white`() {
        me.stoneColor(Stone.Color.EMPTY)

        ReversiBindings.doRejectWhiteAnimation(mockView, reject = me, stamp = System.currentTimeMillis())

        verifyShakeAnimation(never())
    }

    @Test
    fun `it should animate black reject() if reject === me && me === black`() {
        me.stoneColor(Stone.Color.BLACK)

        ReversiBindings.doRejectBlackAnimation(mockView, reject = me, stamp = System.currentTimeMillis())

        verifyShakeAnimation(times(1))
    }

    @Test
    fun `it should animate black reject() if reject === me && me != black`() {
        me.stoneColor(Stone.Color.WHITE)

        ReversiBindings.doRejectBlackAnimation(mockView, reject = me, stamp = System.currentTimeMillis())

        verifyShakeAnimation(never())
    }

    private fun verifyAnimateTo(scale: Float, alpha: Float) {
        verify(mockView).animate()
        verify(mockViewPropertyAnimator).scaleX(scale)
        verify(mockViewPropertyAnimator).scaleY(scale)
        verify(mockViewPropertyAnimator).alpha(alpha)
    }

    private fun verifyShakeAnimation(verificationMode: VerificationMode = times(1)) {
        verify(mockView, verificationMode).startAnimation(null)
    }
}