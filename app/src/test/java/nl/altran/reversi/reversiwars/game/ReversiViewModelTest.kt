package nl.altran.reversi.reversiwars.game

import nl.altran.reversi.reversiwars.game.ui.ReversiViewModel
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ReversiViewModelTest {
    @InjectMocks lateinit var subject: ReversiViewModel

    @Test
    fun `it should have nonnull Game`() {
        assertNotNull(subject.scoreBoard)
    }
}