package nl.altran.reversi.reversiwars.model

import android.os.Handler
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.reset
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import nl.altran.reversi.data.Move
import nl.altran.reversi.reversiwars.model.board.Board
import nl.altran.reversi.reversiwars.model.players.HumanPlayer
import org.junit.Assert
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

@RunWith(MockitoJUnitRunner::class)
class RefereeTest {

    @Mock private lateinit var mockRefereeCallback: RefereeCallback
    @Mock private lateinit var mockForeground: Handler
    @Mock private lateinit var mockBackground: ScheduledExecutorService
    @Mock private lateinit var black: TestHumanPlayer
    @Mock private lateinit var white: TestHumanPlayer

    @Captor private lateinit var bgRunnable: ArgumentCaptor<Runnable>
    @Captor private lateinit var fgRunnable: ArgumentCaptor<Runnable>

    var board = Board()
    lateinit var subject: Referee

    @Before
    fun setUp() {

        whenever(black.color()).thenReturn(Stone.Color.BLACK)
        whenever(white.color()).thenReturn(Stone.Color.WHITE)

        subject = Referee(board, mockForeground, mockBackground)
    }

    @Test
    fun `it should have non-null board`() {
        Assert.assertNotNull(subject.board)
    }

    @Test(expected = UninitializedPropertyAccessException::class)
    fun `it should not initialize current player by default`() {
        subject.isPlayersTurn(HumanPlayer())
    }

    @Test
    fun `it should initialize players when start()`() {
        simulateStart(white, black)
        Assert.assertEquals(listOf(white, black), subject.players)
    }

    @Test
    fun `it should set 1st player as current after start()`() {
        simulateStart(white, black)
        Assert.assertTrue(subject.isPlayersTurn(white))
    }

    @Test
    fun `it should not set 2nd player as current after start()`() {
        simulateStart(white, black)
        Assert.assertFalse(subject.isPlayersTurn(black))
    }

    @Test
    fun `it should toggle between players (1)`() {
        simulateStart(black, white)
        subject.toggle()
        Assert.assertEquals(white, subject.currentPlayer)
    }

    @Test
    fun `it should toggle between players (2)`() {
        simulateStart(white, black)
        subject.toggle()
        Assert.assertEquals(black, subject.currentPlayer)
    }

    @Ignore("How to fix .toggle().toggle() npe?")
    @Test
    fun `it should toggle multiple times between players`() {
        simulateStart(black, white)
        subject.toggle().toggle().toggle().toggle()
        Assert.assertEquals(black, subject.currentPlayer)
    }

    @Test
    fun `it should notify next player after toggle-ing`() {
        simulateStart(black, white)
        subject.toggle()
        verifyNotifyOnBackground()
    }

    @Test
    fun `it should notify current player on background`() {
        simulateStart(white, black)
        subject.notifyCurrentPlayer(white)
        verifyNotifyOnBackground()
    }

    @Test
    fun `it should notify rejected player on background`() {
        simulateStart(white, black)
        subject.notifyPlayerMoveRejected(white)
        verifyRejectedOnBackground()
    }

    @Test
    fun `it should handle submitted moves on foreground`() {
        val move = Move(0, 0)
        val flipped = listOf(Stone(1, 1))

        simulateStart(white, black)
        subject.doSubmitMove(white, move, flipped)

        verify(mockForeground).post(fgRunnable.capture())
        fgRunnable.value.run()

        verify(mockRefereeCallback).submitMove(white, move, flipped)
    }

    @Test
    fun `it should handle rejected moves on foreground`() {
        val move = Move(0, 0)

        simulateStart(white, black)
        subject.doRejectMove(white, move)

        verify(mockForeground).post(fgRunnable.capture())
        fgRunnable.value.run()

        verify(mockRefereeCallback).rejectMove(white, move)
    }

    private fun simulateStart(vararg players: Player) {
        subject.start("guid", players.asList(), mockRefereeCallback)
        verifyNotifyOnBackground(players[0])
        reset(mockBackground)
    }

    private fun verifyNotifyOnBackground(player: Player = white) {
        verify(mockBackground).schedule(bgRunnable.capture(), eq(750L), eq(TimeUnit.MILLISECONDS))
        bgRunnable.value.run()
        verify(player).yourTurn(board.toJson())
        reset(player)
    }

    private fun verifyRejectedOnBackground(player: Player = white) {
        verify(mockBackground).schedule(bgRunnable.capture(), eq(100L), eq(TimeUnit.MILLISECONDS))
        bgRunnable.value.run()
        verify(player).onMoveRejected(board.toJson())
        reset(player)
    }
}