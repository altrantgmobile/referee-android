package nl.altran.reversi.reversiwars

import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.CoordinatorLayout
import android.view.View
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations.initMocks
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class FragmentSwapperBindingTest {
    @Mock private lateinit var mockView: View
    @Mock private lateinit var mockCallback: BottomSheetToggler
    @Mock private lateinit var mockLayoutParameters: CoordinatorLayout.LayoutParams
    @Mock private lateinit var mockBehavior: BottomSheetBehavior<*>

    @Before
    fun setUp() {
        initMocks(this)

        whenever(mockView.layoutParams).thenReturn(mockLayoutParameters)
        whenever(mockLayoutParameters.behavior).thenReturn(mockBehavior)
    }

    @Test
    fun expandInitially() {
        FragmentSwapperBinding.expandInitially(mockView, mockCallback)
    }

    @Test
    fun swapFragments() {
    }

}