package nl.altran.reversi.reversiwars.model.players

import org.junit.Assert
import org.junit.Assert.assertFalse
import org.junit.Test

class HumanPlayerTest {

    @Test
    fun `it should have 'Human' as name`() {
        Assert.assertEquals("Human", HumanPlayer().name())
    }

    @Test
    fun `human() should return true`() {
        Assert.assertTrue(HumanPlayer().human())
    }

    @Test
    fun `local() should return false`() {
        Assert.assertFalse(HumanPlayer().local())
    }

    @Test
    fun `equals() should look at instance level`() {
        assertFalse(HumanPlayer() == HumanPlayer("Piet"))
        assertFalse(HumanPlayer("") == HumanPlayer())
        assertFalse(HumanPlayer("") === HumanPlayer(""))
        assertFalse(HumanPlayer() === HumanPlayer())
        assertFalse(HumanPlayer() == HumanPlayer())
    }
}