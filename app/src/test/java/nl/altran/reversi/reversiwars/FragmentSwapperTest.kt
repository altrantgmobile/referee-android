package nl.altran.reversi.reversiwars

import android.annotation.SuppressLint
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.transition.TransitionSet
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import nl.altran.reversi.reversiwars.game.scoreboard.ScoreFragment
import nl.altran.reversi.reversiwars.select_players.SelectPlayersFragment
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class FragmentSwapperTest {

    lateinit var subject: FragmentSwapper
    @Mock private lateinit var mockFrag: FragmentManager
    @Mock private lateinit var mockTransitionSet: TransitionSet
    @Mock private lateinit var mockTransaction: FragmentTransaction

    @SuppressLint("CommitTransaction")
    @Before
    fun setUp() {
        whenever(mockTransitionSet.clone()).thenReturn(mockTransitionSet)
        whenever(mockTransitionSet.addTransition(any())).thenReturn(mockTransitionSet)
        whenever(mockTransitionSet.excludeTarget(anyInt(), any())).thenReturn(mockTransitionSet)
        whenever(mockFrag.beginTransaction()).thenReturn(mockTransaction)
        whenever(mockTransaction.replace(any(), any(), any())).thenReturn(mockTransaction)

        subject = FragmentSwapper(mockFrag, mockTransitionSet)
    }

    @Test
    fun `it should add ScoreFragment when swap(true)`() {
        subject.swap(true)

        verify(mockTransaction).replace(eq(R.id.container), any<ScoreFragment>(), any())
    }

    @Test
    fun `it should add SelectPlayerFragment when swap(false)`() {
        subject.swap(false)

        verify(mockTransaction).replace(eq(R.id.container), any<SelectPlayersFragment>(), any())
    }

    @Test
    fun `it should set isPlaying(true) when state collapsed`() {
        subject.onToggleAndSlide(BottomSheetBehavior.STATE_COLLAPSED, 0F)
        Assert.assertTrue(subject.isPlaying.get())
    }

    @Test
    fun `it should set isPlaying(false) when state anything other than collapsed`() {
        subject.onToggleAndSlide(BottomSheetBehavior.STATE_EXPANDED, 0F)
        Assert.assertFalse(subject.isPlaying.get())

        subject.onToggleAndSlide(BottomSheetBehavior.STATE_DRAGGING, 0F)
        Assert.assertFalse(subject.isPlaying.get())

        subject.onToggleAndSlide(BottomSheetBehavior.STATE_HIDDEN, 0F)
        Assert.assertFalse(subject.isPlaying.get())

        subject.onToggleAndSlide(BottomSheetBehavior.STATE_SETTLING, 0F)
        Assert.assertFalse(subject.isPlaying.get())
    }
}