package nl.altran.reversi.reversiwars.select_players

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import nl.altran.reversi.reversiwars.game.usecases.MatchNode
import nl.altran.reversi.reversiwars.model.Player
import nl.altran.reversi.reversiwars.model.Stone
import nl.altran.reversi.reversiwars.select_players.usecases.CreateMatchUsecase
import nl.altran.reversi.reversiwars.select_players.usecases.MatchRepository
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CreateMatchUsecaseTest{

    @InjectMocks lateinit var subject: CreateMatchUsecase
    @Mock private lateinit var mockCallback: CreateMatchUsecase.Callback
    @Mock private lateinit var mockRepo: MatchRepository
    @Mock private lateinit var mockWhite: Player
    @Mock private lateinit var mockBlack: Player

    @Test
    fun `it should set correct stone colors for both players`() {
        subject.go(mockWhite, mockBlack)

        verify(mockWhite).stoneColor(Stone.Color.WHITE)
        verify(mockBlack).stoneColor(Stone.Color.BLACK)
    }

    @Test
    fun `it should be created with match repository`() {
        val matchCaptor = argumentCaptor<MatchNode>()
        whenever(mockWhite.guid()).thenReturn("guid1")
        whenever(mockBlack.guid()).thenReturn("guid2")

        subject.go(mockWhite, mockBlack)

        verify(mockRepo).createMatch(matchCaptor.capture(), any(), any())
        Assert.assertEquals("{\"guid1\": true}", matchCaptor.firstValue.white)
        Assert.assertEquals("{\"guid2\": true}", matchCaptor.firstValue.black)
    }

    @Test
    fun `it should notify callback of success`() {
        subject.onSucces.invoke("1", mockWhite, mockBlack)

        verify(mockCallback).onGameCreated(any())
    }

    @Test
    fun `it should notify callback off failures`() {
        subject.onFail.invoke("Oops")

        verify(mockCallback).onCreateFailed("Oops")
    }
}