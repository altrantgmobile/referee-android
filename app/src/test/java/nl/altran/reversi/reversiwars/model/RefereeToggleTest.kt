package nl.altran.reversi.reversiwars.model

import android.os.Handler
import nl.altran.reversi.reversiwars.model.board.Board
import nl.altran.reversi.reversiwars.model.players.HumanPlayer
import nl.altran.reversi.reversiwars.model.players.RandomPlayer
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.ScheduledExecutorService

@RunWith(MockitoJUnitRunner::class)
class RefereeToggleTest {

    @Mock private lateinit var mockRefereeCallback: RefereeCallback
    @Mock private lateinit var mockForeground: Handler
    @Mock private lateinit var mockBackground: ScheduledExecutorService

    private val randomWhite = RandomPlayer()
    private val randomBlack = RandomPlayer()
    private val humanWhite = HumanPlayer()
    private val humanBlack = HumanPlayer()


    var board = Board()
    lateinit var subject: Referee

    @Before
    fun setUp() {

        randomWhite.stoneColor(Stone.Color.WHITE)
        randomBlack.stoneColor(Stone.Color.BLACK)

        subject = Referee(board, mockForeground, mockBackground)
        subject.start("guid", listOf(randomWhite, randomBlack), mockRefereeCallback)
    }

    @Test
    fun `it should toggle RandomPlayers()`() {
        subject.toggle()
        Assert.assertEquals(randomBlack, subject.currentPlayer)
    }

    @Test
    fun `it should toggle random player mutiple times`() {
        subject.toggle().toggle()
        Assert.assertEquals(randomWhite, subject.currentPlayer)
    }

    @Test
    fun `it should toggle random player mutiple+ times`() {
        subject.toggle().toggle().toggle()
        Assert.assertEquals(randomBlack, subject.currentPlayer)
    }

    @Test
    fun `it should not be equal() for RandomPlayer()`() {
        Assert.assertNotEquals(randomWhite, randomBlack)
        Assert.assertFalse(randomWhite == randomBlack)
        Assert.assertFalse(randomWhite === randomBlack)
    }

    @Test
    fun `it should not be equal() for HumanPlayer()`() {
        Assert.assertNotEquals(humanWhite, humanBlack)
//        Assert.assertFalse(humanWhite == humanBlack)
//        Assert.assertFalse(humanWhite === humanBlack)
    }

    @Test
    fun `it should have different indices for different players`() {
        val whiteIndex = subject.players.indexOf(randomWhite)
        val blackIndex = subject.players.indexOf(randomBlack)
        Assert.assertNotEquals(whiteIndex, blackIndex)
    }
}