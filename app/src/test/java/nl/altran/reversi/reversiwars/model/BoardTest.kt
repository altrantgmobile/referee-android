package nl.altran.reversi.reversiwars.model

import nl.altran.reversi.data.Move
import nl.altran.reversi.reversiwars.model.board.Board
import org.junit.Assert.*
import org.junit.Test

class BoardTest {
    @Test
    fun get() {
        val board = Board(8)
        assertEquals(Stone.Color.EMPTY, board.get(0, 0).color.get())
        assertEquals(Stone.Color.EMPTY, board.get(0, 1).color.get())
        assertEquals(Stone.Color.EMPTY, board.get(1, 0).color.get())
        assertEquals(Stone.Color.WHITE, board.get(3, 3).color.get())
        assertEquals(Stone.Color.BLACK, board.get(3, 4).color.get())
        assertEquals(Stone.Color.BLACK, board.get(4, 3).color.get())
        assertEquals(Stone.Color.WHITE, board.get(4, 4).color.get())
    }

    @Test
    fun getIndex() {
        val board = Board(8)
        assertEquals(Stone.Color.EMPTY, board.get(0).color.get())
        assertEquals(Stone.Color.EMPTY, board.get(1).color.get())
        assertEquals(Stone.Color.EMPTY, board.get(63).color.get())
        assertEquals(Stone.Color.WHITE, board.get(27).color.get())
        assertEquals(Stone.Color.BLACK, board.get(28).color.get())
        assertEquals(Stone.Color.BLACK, board.get(27 + 8).color.get())
        assertEquals(Stone.Color.WHITE, board.get(28 + 8).color.get())
    }

    @Test
    fun update() {
        val board = Board(8)
        board.set(board.get(0), Stone.Color.BLACK)
        assertEquals(Stone.Color.BLACK, board.get(0).color.get())
    }

    @Test
    fun update2() {
        val board = Board(8)
        board.set(board.get(3), Stone.Color.BLACK)
        assertEquals(Stone.Color.BLACK, board.get(0, 3).color.get())
    }

    @Test
    fun update3() {
        val board = Board(8)
        board.set(board.get(7, 0), Stone.Color.BLACK)
        assertEquals(Stone.Color.BLACK, board.get(56).color.get())
    }

    @Test
    fun isEmpty() {
        assertTrue(Board(8).isEmpty(Move(0, 0)))
    }

    @Test
    fun restart() {
    }

    @Test
    fun `it should have default size of 8`() {
        assertEquals(8, Board().size)
    }

    @Test
    fun `it should flip left`() {
        val flipped = Board(8).applyMove(Move(4, 5), Stone.Color.BLACK)
        assertEquals(1, flipped.size)
    }

    @Test
    fun `it should flip up`() {
        val flipped = Board(8).applyMove(Move(5, 4), Stone.Color.BLACK)
        assertEquals(1, flipped.size)
    }

    @Test
    fun `it should flip right`() {
        val flipped = Board(8).applyMove(Move(3, 5), Stone.Color.WHITE)
        assertEquals(1, flipped.size)
    }

    @Test
    fun `it should flip down`() {
        val flipped = Board(8).applyMove(Move(5, 3), Stone.Color.WHITE)
        assertEquals(1, flipped.size)
    }

    @Test
    fun `it should clone the stones`() {
        val board = Board()
        val clone = board.clone()
        assertEquals(emptyList<Stone>(), clone.applyMove(Move(1, 1), Stone.Color.WHITE))
        assertNotEquals(board, clone)
    }

    @Test
    fun `it should print board to json correct`() {
        assertEquals("{\"size\":8,\"board\":[[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,1,-1,0,0,0],[0,0,0,-1,1,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0]]}", Board().toJson())
    }
}