package nl.altran.reversi.reversiwars.game

import nl.altran.reversi.reversiwars.game.scoreboard.ScoreBoard
import nl.altran.reversi.reversiwars.model.Stone
import nl.altran.reversi.reversiwars.model.players.HumanPlayer
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class ScoreBoardTest {

    lateinit var subject: ScoreBoard

    private val white = HumanPlayer()
    private val black = HumanPlayer()

    @Before
    fun setUp() {
        white.stoneColor(Stone.Color.WHITE)
        black.stoneColor(Stone.Color.BLACK)

        subject = ScoreBoard("guid", white, black)
    }

    @Test
    fun `it should set white player as current by default`() {
        Assert.assertEquals(white, subject.currentPlayer)
    }

    @Test
    fun `it should toggle players`() {
        Assert.assertEquals(white, subject.currentPlayer)
        subject.togglePlayers()
        Assert.assertEquals(black, subject.currentPlayer)
        subject.togglePlayers()
        Assert.assertEquals(white, subject.currentPlayer)
        subject.togglePlayers()
        Assert.assertEquals(black, subject.currentPlayer)
        subject.togglePlayers()
        Assert.assertEquals(white, subject.currentPlayer)
        subject.togglePlayers()
        Assert.assertEquals(black, subject.currentPlayer)
    }

    @Test
    fun `it should set current player at start`() {
        subject.togglePlayers()
        subject.onStart()
        Assert.assertEquals(white, subject.currentPlayer)
    }

    @Test
    fun `it should reset scores to initial values after start()`() {
        subject.onStart()
        Assert.assertEquals(ScoreBoard.INITIAL_SCORE, subject.scoreWhite.get())
        Assert.assertEquals(ScoreBoard.INITIAL_SCORE, subject.scoreBlack.get())
    }

    @Test
    fun `it should toggle players once white submits move`() {
        subject.onMoveSubmitted(white, 1)
        Assert.assertEquals(black, subject.currentPlayer)
    }

    @Test
    fun `it should toggle players once black submits move`() {
        subject.onMoveSubmitted(black, 3)
        Assert.assertEquals(white, subject.currentPlayer)
    }

    @Test
    fun `it should update score when white flipped 6 stones`() {
        subject.onStart()
        subject.onMoveSubmitted(white, 6)

        Assert.assertEquals(2 + 6 + 1, subject.scoreWhite.get())
        Assert.assertEquals(2 - 6, subject.scoreBlack.get())
    }

    @Test
    fun `it should store rejectedPlayer when move is rejected`() {
        subject.onMoveRejected(white)
        Assert.assertEquals(white, subject.rejectedPlayer.get())
    }

    @Test
    fun `it should update timestamp of rejected move, to trigger reject animation`() {
        val before = subject.rejectedStamp.get()
        subject.onMoveRejected(black)
        Assert.assertNotEquals(before, subject.rejectedStamp.get())
    }
}

