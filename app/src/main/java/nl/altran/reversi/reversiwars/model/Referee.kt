package nl.altran.reversi.reversiwars.model

import android.databinding.ObservableField
import android.os.Handler
import android.os.Looper
import nl.altran.reversi.data.Move
import nl.altran.reversi.reversiwars.game.scoreboard.GameTimer
import nl.altran.reversi.reversiwars.game.scoreboard.GameTimerCallback
import nl.altran.reversi.reversiwars.model.board.Board
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit


class Referee(val board: Board,
              private val foreground: Handler = Handler(Looper.getMainLooper()),
              private val background: ScheduledExecutorService = Executors.newScheduledThreadPool(2))
    : AnkoLogger, PlayerCallback, GameTimerCallback {

    private val timer: GameTimer = GameTimer()

    override lateinit var currentPlayer: Player
    private lateinit var refereeCallback: RefereeCallback
    internal lateinit var players: List<Player>

    val showTimer = ObservableField<Stone.Color>(Stone.Color.EMPTY)

    fun start(matchId: String, players: List<Player>, refCallback: RefereeCallback) {
        this.refereeCallback = refCallback
        this.board.restart()
        setupPlayers(matchId, players, this)
    }

    private fun setupPlayers(matchId: String, players: List<Player>, playersCallback: PlayerCallback) {
        this.players = players
        this.players.forEach {
            it.playerCallback(playersCallback)
            it.onJoinedGame(matchId)
        }
        this.currentPlayer = players[0]
        notifyCurrentPlayer(this.currentPlayer)
    }

    internal fun notifyCurrentPlayer(player: Player) {
        startTimer(player)
        background.schedule(
                {
                    info("onYourTurn (player: $player)")
                    player.yourTurn(board.toJson())
                }, 750, TimeUnit.MILLISECONDS
        )
    }

    internal fun notifyPlayerMoveRejected(player: Player) {
        background.schedule(
                {
                    info("Move rejected (player: $player)")
                    player.onMoveRejected(board.toJson())
                }, 100, TimeUnit.MILLISECONDS
        )
    }

    internal fun notifyGameFinished(player: Player, yourScore: Int, opponentScore: Int) {
        background.schedule(
                {
                    info("Game finished (player: $player) $yourScore, $opponentScore")
                    player.onGameFinished(yourScore, opponentScore)
                }, 100, TimeUnit.MILLISECONDS
        )
    }

    fun toggle(): Referee {
        if (board.canMove(swap().color()) || board.canMove(swap().color())) {
            notifyCurrentPlayer(currentPlayer)
        } else {
            // Game Finished
            doFinishGame()
        }
        return this
    }

    private fun swap(): Player {
        val nextPlayerIndex = (players.indexOf(currentPlayer) + 1) % (players.size)
        currentPlayer = players[nextPlayerIndex]
        return currentPlayer
    }

    private fun tryMove(move: Move, color: Stone.Color): List<Stone> {
        return applyMove(board.clone(), move, color)
    }

    private fun applyMove(newBoard: Board = board, move: Move, color: Stone.Color): List<Stone> {
        return newBoard.applyMove(move, color)
    }

    @Synchronized
    fun isPlayersTurn(player: Player): Boolean {
        return currentPlayer === player
    }

    private fun isValidPosition(move: Move): Boolean {
        return inBounds(move) && isEmptySpot(move)
    }

    private fun isEmptySpot(move: Move): Boolean {
        return board.isEmpty(move)
    }

    private fun inBounds(aMove: Move): Boolean {
        if (aMove.col < 0 || aMove.col >= board.size()) return false
        if (aMove.row < 0 || aMove.row >= board.size()) return false
        return true
    }

    override fun onMoveSubmitted(player: Player, move: Move) {
        if (isPlayersTurn(player)) {
            if (isValidPosition(move)) {
                val flipped: List<Stone> = tryMove(move, player.color())
                if (!flipped.isEmpty()) {
                    stopTimer()
                    applyMove(move = move, color = player.color())
                    doSubmitMove(player, move, flipped)
                    return
                }
            }

            doRejectMove(player, move)
        }
    }

    internal fun doRejectMove(player: Player, move: Move) {
        notifyPlayerMoveRejected(player)
        foreground.post {
            refereeCallback.rejectMove(player, move)
        }
    }

    internal fun doSubmitMove(player: Player, move: Move, flipped: List<Stone>) {
        foreground.post {
            refereeCallback.submitMove(player, move, flipped)
        }
    }

    private fun doFinishGame() {
        val (scoreWhite, scoreBlack, _) = board.getScore()
        background.schedule({
            players.forEach {
                val myColor = it.color()
                val your = board.stones.filter { it.color.get() == myColor }.size
                val opp = board.stones.filter { it.color.get() != myColor }.size
                notifyGameFinished(it, your, opp)
            }
        }, 1, TimeUnit.MILLISECONDS)
        foreground.post {
            refereeCallback.gameFinished(players[0], scoreWhite, players[1], scoreBlack)
        }
    }


    override fun onTimedOut() {
        foreground.post {
            refereeCallback.timedOut()
        }
    }

    private fun startTimer(player: Player) {
        if (!player.human()) {
            showTimer.set(player.color())
            timer.start(this)
        }
    }

    private fun stopTimer() {
        showTimer.set(Stone.Color.EMPTY)
        timer.stop()
    }
}