package nl.altran.reversi.reversiwars.model.players

import nl.altran.reversi.data.Move

class RandomPlayer(private val name: String = "Random") : BasePlayer() {

    override fun name(): String {
        return name
    }

    override fun human(): Boolean {
        return false
    }

    override fun local(): Boolean {
        return true
    }

    override fun onYourTurn(board: String) {
        val row = (Math.random() * 8).toInt()
        val col = (Math.random() * 8).toInt()
        submitMove(Move(row, col))
    }

    override fun onRejected(board: String) {
        // Simply try again
        onYourTurn(board)
    }

    override fun toString(): String {
        return "RandomPlayer(name='$name', '${color()}')"
    }
}