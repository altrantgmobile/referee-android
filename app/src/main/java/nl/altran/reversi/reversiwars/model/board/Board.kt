package nl.altran.reversi.reversiwars.model.board

import android.databinding.ObservableField
import nl.altran.reversi.data.Move
import nl.altran.reversi.reversiwars.model.Score
import nl.altran.reversi.reversiwars.model.Stone
import org.jetbrains.anko.AnkoLogger

data class Board(val size: Int = 8, private val init: BoardInitializer = BoardInitializer(), val stones: MutableList<Stone> = init.init(size)) : AnkoLogger {

    fun get(index: Int): Stone {
        return stones[index]
    }

    fun get(move: Move): Stone {
        return get(move.row, move.col)
    }

    fun get(row: Int, col: Int): Stone {
        return stones[row * size + col]
    }

    fun set(stone: Stone, color: Stone.Color): Stone {
        val stone1 = get(stone.row, stone.col)
        stone1.set(color)
        return stone1
    }

    fun set(index: Int, element: Stone): Stone {
        return stones.set(index, element)
    }

    fun isEmpty(move: Move): Boolean {
        return get(move.row, move.col).color.get() == Stone.Color.EMPTY
    }

    fun size(): Int {
        return size
    }

    fun restart() {
        stones.clear()
        stones.addAll(init.init(size))
    }

    fun applyMove(move: Move, stone: Int): List<Stone> {
        val stoneColor = Stone.Color.fromInt(stone)
        return applyMove(move, stoneColor)
    }

    fun applyMove(move: Move, stone: Stone.Color): List<Stone> {
        set(get(move.row, move.col), stone)
        return BoardFlipper(this).flip(move, stone)
    }

    fun getScore(): Score {
        return Score(
                stones.filter { it.color.get() == Stone.Color.WHITE }.size,
                stones.filter { it.color.get() == Stone.Color.BLACK }.size,
                stones.filter { it.color.get() == Stone.Color.EMPTY }.size)
    }

    fun canMove(color: Stone.Color): Boolean {
        val potentialMoves = emptyList<Move>().toMutableList()
        clone().stones.filter { it.color.get() == Stone.Color.EMPTY }.forEach { potentialMoves.add(Move(it.row, it.col)) }
        potentialMoves.forEach {
            if (clone().applyMove(it, color).isNotEmpty())
                return true
        }
        return false
    }

    fun clone(): Board {
        return Board(size = size, stones = stones.map { Stone(it.row, it.col, ObservableField(it.color.get()!!)) }.toMutableList())
    }

    fun setupWithData(raw: Array<IntArray>): Board {
        restart()
        for (row in raw.indices) {
            for (col in 0 until raw[row].size) {
                val color = raw[row][col]
                set(Stone(row, col), Stone.Color.fromInt(color))
            }
        }
        return this
    }

    override fun toString(): String {
        val builder = StringBuilder()
        builder.append("|")
        for (rows in 0 until size) {
            for (cols in 0 until size) {
                val stone = get(rows, cols).color.get()
                when (stone!!) {
                    Stone.Color.WHITE -> builder.append("w")
                    Stone.Color.BLACK -> builder.append("b")
                    Stone.Color.EMPTY -> builder.append("_")
                }

                if (cols < size - 1) {
                    builder.append("|")
                }
            }
            builder.append("|\n")
            if (rows < size - 1) {
                builder.append("|")
            }
        }
        return builder.toString()
    }

    fun toJson(): String {
        val builder = StringBuilder("{\"size\":")
        builder.append(size)
        builder.append(",")
        builder.append("\"board\":")
        builder.append("[")
        for (row in 0 until size) {
            builder.append("[")
            for (col in 0 until size) {
                builder.append(get(row, col).color.get()!!.asInt())
                if (col < size - 1) {
                    builder.append(",")
                }
            }
            builder.append("]")
            if (row < size - 1) {
                builder.append(",")
            }
        }
        return builder.append("]}").toString()
    }
}
