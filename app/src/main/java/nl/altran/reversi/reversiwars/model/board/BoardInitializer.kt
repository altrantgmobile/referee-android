package nl.altran.reversi.reversiwars.model.board

import android.databinding.ObservableField
import nl.altran.reversi.reversiwars.model.Stone

class BoardInitializer {
    fun init(size: Int): MutableList<Stone> {
        return Array(size * size, { it -> stoneAtPosition(it, size) }).toMutableList()
    }

    private fun stoneAtPosition(it: Int, size: Int): Stone {
        val row = it / size
        val col = it % size
        val color = colorAtPosition(row, col, size)
        return Stone(row, col, ObservableField(color))
    }

    private fun colorAtPosition(row: Int, col: Int, size: Int): Stone.Color {
        val fst = size / 2 - 1
        val scd = size / 2
        if (row == fst && col == fst) return Stone.Color.WHITE
        if (row == fst && col == scd) return Stone.Color.BLACK
        if (row == scd && col == fst) return Stone.Color.BLACK
        if (row == scd && col == scd) return Stone.Color.WHITE
        return Stone.Color.EMPTY
    }
}