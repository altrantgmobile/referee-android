package nl.altran.reversi.reversiwars.model

import me.tatarka.bindingcollectionadapter2.ItemBinding
import me.tatarka.bindingcollectionadapter2.itembindings.ItemBindingModel
import nl.altran.reversi.reversiwars.BR
import nl.altran.reversi.reversiwars.R

interface Player : ItemBindingModel {

    fun guid(): String
    fun name(): String
    fun human(): Boolean
    fun local(): Boolean
    fun playerCallback(playerCallback: PlayerCallback)
    fun onJoinedGame(guid: String)

    fun yourTurn(board: String)
    fun onMoveRejected(board: String)
    fun onGameFinished(yourScore: Int, opponentScore: Int)
    fun stoneColor(color: Stone.Color)
    fun color(): Stone.Color

    override fun onItemBind(itemBinding: ItemBinding<*>?) {
        itemBinding!!.set(BR.player, R.layout.item_player)
    }
}