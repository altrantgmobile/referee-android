package nl.altran.reversi.reversiwars.game.ui

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import nl.altran.reversi.reversiwars.R
import nl.altran.reversi.reversiwars.databinding.FragmentReversiBinding

/**
 * A placeholder fragment containing a simple view.
 */
class ReversiFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentReversiBinding>(inflater, R.layout.fragment_reversi, container, false)
        binding.reversi = ViewModelProviders.of(activity!!).get(ReversiViewModel::class.java)
        return binding.root
    }
}
