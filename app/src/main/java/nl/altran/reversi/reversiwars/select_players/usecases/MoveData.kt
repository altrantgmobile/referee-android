package nl.altran.reversi.reversiwars.select_players.usecases

import nl.altran.reversi.data.Move
import nl.altran.reversi.reversiwars.model.Player

data class MoveData(val player: Player, val move: Move)