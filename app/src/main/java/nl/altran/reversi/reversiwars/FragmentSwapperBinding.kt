package nl.altran.reversi.reversiwars

import android.databinding.BindingAdapter
import android.support.design.widget.BottomSheetBehavior
import android.view.View

object FragmentSwapperBinding {
    @BindingAdapter("bottomSheetToggler")
    @JvmStatic
    fun expandInitially(view: View, callback: BottomSheetToggler) {
        val behavior = BottomSheetBehavior.from(view)
        behavior.setBottomSheetCallback(callback)
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    @BindingAdapter("fragmentSwapper", "fragmentIndex")
    @JvmStatic
    fun swapFragments(view: View, swapper: FragmentSwapper?, isPlaying: Boolean) {
        val behavior = BottomSheetBehavior.from(view)
        behavior.state = if (isPlaying) BottomSheetBehavior.STATE_COLLAPSED else BottomSheetBehavior.STATE_EXPANDED
        swapper?.swap(isPlaying)
    }
}