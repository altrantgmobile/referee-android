package nl.altran.reversi.reversiwars.select_players

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.databinding.ObservableList
import me.tatarka.bindingcollectionadapter2.ItemBinding
import me.tatarka.bindingcollectionadapter2.itembindings.OnItemBindModel
import nl.altran.reversi.reversiwars.BR
import nl.altran.reversi.reversiwars.model.Player
import nl.altran.reversi.reversiwars.select_players.usecases.CreateMatchUsecase
import nl.altran.reversi.reversiwars.select_players.usecases.FetchPlayersUsecase
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug
import javax.inject.Inject


class SelectPlayersViewModel
@Inject
constructor(private val lifecycle: Lifecycle,
            private val fetchPlayersUsecase: FetchPlayersUsecase,
            private val createMatchUsecase: CreateMatchUsecase)

    : ViewModel(), LifecycleObserver, AnkoLogger, OnPlayerSelectListener, FetchPlayersUsecase.Callback {

    private val playerBinding: OnItemBindModel<Player> = OnItemBindModel()

    val players: ObservableList<Player> = ObservableArrayList()
    val player: ItemBinding<Player> = ItemBinding.of(playerBinding).bindExtra(BR.playerClicker, this)
    val white: ObservableField<Player> = ObservableField()
    val black: ObservableField<Player> = ObservableField()
    val current = ObservableInt(0)

    init {
        lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(value = Lifecycle.Event.ON_RESUME)
    fun onStart() {
        debug("onStart $lifecycle")
        fetchPlayersUsecase.registerCallback(this)
    }

    @OnLifecycleEvent(value = Lifecycle.Event.ON_PAUSE)
    fun onStop() {
        debug("onStop $lifecycle")
        fetchPlayersUsecase.unregisterCallback()
        lifecycle.removeObserver(this)
    }

    override fun onPlayerRetrieved(player: Player) {
        if(!players.contains(player)) {
            players.add(player)
        }
    }

    override fun onPlayerUpdated(player: Player) {
        val index = players.indexOfFirst { isSame(it, player) }
        if (index >= 0) {
            players[index] = player
        }
    }

    private fun isSame(it: Player, player: Player) =
            it.guid() == player.guid()

    override fun onPlayerRemoved(name: String) {
        players.removeAll(players.filter { it.name() == name })
    }

    override fun onPlayerSelected(player: Player) {

        players.remove(player)

        if (white.get() == null) {
            current.set(1)
            white.set(player)
            createMatchUsecase.clear()
        } else if (black.get() == null) {
            current.set(2)
            black.set(player)
            createMatchUsecase.go(white.get()!!, black.get()!!)
        }
    }
}

