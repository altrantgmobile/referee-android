package nl.altran.reversi.reversiwars.game.usecases

import com.google.firebase.database.Exclude
import com.google.gson.annotations.SerializedName
import nl.altran.reversi.data.Move
import nl.altran.reversi.reversiwars.model.Player

data class MatchNode(@get:Exclude internal val p1: Player, @get:Exclude internal val p2: Player) {
    @SerializedName("white")
    val white = "{\"${p1.guid()}\": true}"
    @SerializedName("black")
    val black = "{\"${p2.guid()}\": true}"
    @SerializedName("moves")
    val moves = emptyList<Move>()
}