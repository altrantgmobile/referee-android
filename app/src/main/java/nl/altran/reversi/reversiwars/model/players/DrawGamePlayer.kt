package nl.altran.reversi.reversiwars.model.players

import nl.altran.reversi.reversiwars.model.Stone

class DrawGamePlayer : BasePlayer() {
    override fun name(): String {
        return when(stoneColor){
            Stone.Color.EMPTY -> "Draw"
            else -> "Draw $stoneColor"
        }
    }

    override fun human(): Boolean {
        return false
    }

    override fun local(): Boolean {
        return true
    }

    override fun onYourTurn(board: String) {}

    override fun onRejected(board: String) {}
}