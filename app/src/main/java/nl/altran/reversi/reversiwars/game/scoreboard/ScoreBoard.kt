package nl.altran.reversi.reversiwars.game.scoreboard

import android.databinding.ObservableField
import android.databinding.ObservableInt
import nl.altran.reversi.reversiwars.model.Player
import nl.altran.reversi.reversiwars.model.Stone
import nl.altran.reversi.reversiwars.model.players.DrawGamePlayer
import org.jetbrains.anko.AnkoLogger

/**
 * Keeps track of the score only!
 * Scoreboard is not aware of the rules e.g.
 * it does not check if moves are legal/valid or
 * if the player submitting the move was allowed to...
 *
 * It does what it is told to do
 */
class ScoreBoard(val matchId: String, val white: Player, val black: Player) : AnkoLogger {

    companion object {
        const val INITIAL_SCORE: Int = 2
    }

    val scoreWhite = ObservableInt(0)
    val scoreBlack = ObservableInt(0)
    val current = ObservableField<Player>()
    val winner = ObservableField<Player?>()
    val rejectedPlayer = ObservableField<Player>()
    val rejectedStamp = ObservableField<Long>(0)

    var currentPlayer: Player = white
        set(value) {
            if (field !== value) {
                field = value
                current.set(value)
            }
        }

    fun onStart(): ScoreBoard {
        currentPlayer = white
        scoreWhite.set(INITIAL_SCORE)
        scoreBlack.set(INITIAL_SCORE)
        winner.set(null)
        rejectedPlayer.set(null)
        rejectedStamp.set(0)
        return this
    }

    fun onMoveSubmitted(player: Player, stonesFlipped: Int) {
        togglePlayers(player)
        updateScores(player, stonesFlipped)
    }

    fun onMoveRejected(player: Player) {
        rejectedPlayer.set(player)
        rejectedStamp.set(System.currentTimeMillis())
    }

    private fun updateScores(player: Player, stonesFlipped: Int) {
        if (player.color() == Stone.Color.WHITE) {
            scoreWhite.set(scoreWhite.get() + 1 + stonesFlipped)
            scoreBlack.set(scoreBlack.get() - stonesFlipped)
        } else if (player.color() == Stone.Color.BLACK) {
            scoreBlack.set(scoreBlack.get() + 1 + stonesFlipped)
            scoreWhite.set(scoreWhite.get() - stonesFlipped)
        }
    }

    internal fun togglePlayers(player: Player = currentPlayer) {
        if (player === white) {
            currentPlayer = black
        } else {
            currentPlayer = white
        }
    }

    fun players(): List<Player> {
        return listOf(white, black)
    }

    fun onGameFinished(white: Player, whiteScore: Int, black: Player, blackScore: Int) {
        scoreWhite.set(whiteScore)
        scoreBlack.set(blackScore)

        val whosBadAss = if (whiteScore > blackScore) white else if (whiteScore < blackScore) black else DrawGamePlayer()
        winner.set(whosBadAss)
        currentPlayer = whosBadAss
    }
}