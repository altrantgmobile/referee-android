package nl.altran.reversi.reversiwars.select_players.usecases

import nl.altran.reversi.reversiwars.game.scoreboard.ScoreBoard
import nl.altran.reversi.reversiwars.game.usecases.MatchNode
import nl.altran.reversi.reversiwars.model.Player
import nl.altran.reversi.reversiwars.model.Stone
import javax.inject.Inject

class CreateMatchUsecase @Inject constructor(val callback: Callback, private val repo: MatchRepository) {

    interface Callback {
        fun onGameCreated(scoreBoard: ScoreBoard)
        fun onCreateFailed(error: String)
        fun onClear()
    }

    internal val onSucces: (String, Player, Player) -> Unit = { matchId, white, black -> callback.onGameCreated(ScoreBoard(matchId, white, black))}
    internal val onFail: (String) -> Unit = { error -> callback.onCreateFailed(error)}

    fun go(whitePlayer: Player, blackPlayer: Player) {
        whitePlayer.stoneColor(Stone.Color.WHITE)
        blackPlayer.stoneColor(Stone.Color.BLACK)
        repo.createMatch(MatchNode(whitePlayer, blackPlayer), onSucces, onFail)
    }

    fun clear() {
        callback.onClear()
    }
}