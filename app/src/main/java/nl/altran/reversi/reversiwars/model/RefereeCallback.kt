package nl.altran.reversi.reversiwars.model

import nl.altran.reversi.data.Move

interface RefereeCallback {
    fun submitMove(player: Player, move: Move, list: List<Stone>)
    fun timedOut()
    fun rejectMove(player: Player, move: Move)
    fun gameFinished(white: Player, scoreWhite: Int, black: Player, scoreBlack: Int)
}

