package nl.altran.reversi.reversiwars.game.ui

import android.databinding.BindingAdapter
import android.graphics.drawable.Drawable
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import nl.altran.reversi.data.Move
import nl.altran.reversi.reversiwars.AnimationHelper
import nl.altran.reversi.reversiwars.R
import nl.altran.reversi.reversiwars.game.scoreboard.CountDownActionButton
import nl.altran.reversi.reversiwars.model.Player
import nl.altran.reversi.reversiwars.model.Stone
import nl.altran.reversi.reversiwars.model.players.DrawGamePlayer
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug
import org.jetbrains.anko.info

object ReversiBindings : AnkoLogger {
    @JvmStatic
    @BindingAdapter("me", "current")
    fun doCurrentPlayerAnimation(view: ImageView, me: Player?, current: Player?) {
        debug("me:$me, current:$current")
        if (current == null || me == null || me === current) {
            // Grow bigger
            view.animate().scaleX(1f).scaleY(1f).alpha(1f).start()
        } else {
            // Grow smaller
            view.animate().scaleX(0.5f).scaleY(0.5f).alpha(0.3f).start()
        }
    }

    @JvmStatic
    @BindingAdapter("rejectWhite", "rejectStamp")
    fun doRejectWhiteAnimation(view: ImageView, reject: Player?, @Suppress("UNUSED_PARAMETER") stamp: Long?) {
        debug("rejectedPlayer:$reject")
        if (reject != null && reject.color() == Stone.Color.WHITE) {
            // Reject Animation
            AnimationHelper.shake(view)
        } else {
            view.rotation = 0F
        }
    }

    @JvmStatic
    @BindingAdapter("rejectBlack", "rejectStamp")
    fun doRejectBlackAnimation(view: ImageView, reject: Player?, stamp: Long?) {
        debug("rejectedPlayer:$reject")
        if (reject != null && reject.color() == Stone.Color.BLACK) {
            // Reject Animation
            AnimationHelper.shake(view)
        } else {
            view.rotation = 0F
        }
    }

    @JvmStatic
    @BindingAdapter("showWinnerAnimation")
    fun doShowWinnerAnimation(view: TextView, winner: Player?) {
        when (winner) {
            null -> {
                // Hide
                view.text = ""
                view.animate().alpha(0F).scaleX(0F).scaleY(0F).withEndAction({ view.visibility = View.INVISIBLE })
            }
            is DrawGamePlayer -> {
                // Tie game
                view.text = view.context.getString(R.string.draw)
                view.animate().withStartAction({ view.visibility = View.VISIBLE }).alpha(1F).scaleX(1F).scaleY(1F)
            }
            else -> {
                // Show Winner name
                view.text = view.context.getString(R.string.winner, winner.name())
                view.animate().withStartAction({ view.visibility = View.VISIBLE }).alpha(1F).scaleX(1F).scaleY(1F)
            }
        }
    }


    @JvmStatic
    @BindingAdapter("hideForWinner")
    fun doHideFabForWinnerAnimation(view: FloatingActionButton, winner: Player?) {
        if (winner == null) {
            // Show Normally
            view.animate().scaleX(1F).scaleY(1F).alpha(1F).start()
        } else {
            // Hide for Winner
            view.animate().scaleX(.5F).scaleY(.5F).alpha(0F).start()
        }
    }

    @JvmStatic
    @BindingAdapter("showFabIcon")
    fun doSetImageSourceForFab(fab: FloatingActionButton, resource: Drawable?) {
        fab.setImageDrawable(resource)
    }

    @JvmStatic
    @BindingAdapter("showTimerAnimation")
    fun doStartTimerAnimation(fab: CountDownActionButton, color: Stone.Color) {
        if (color != Stone.Color.EMPTY) {
            fab.startCountDown(color)
        } else {
            fab.cancelCountDown()
        }
    }

    @JvmStatic
    @BindingAdapter("lastSelected")
    fun showLastSelected(view: RecyclerView, move: Move?){
        if(view.adapter != null && move != null) {
            for (position in 0..view.adapter.itemCount) {
                val stone = view.layoutManager.findViewByPosition(position)
                if(stone != null){
                    val b = move.row * 8 + move.col != position
                    if(!b) {
                        info("position:$position, isEnabled:$b")
                    }
                    stone.isEnabled = b
                }
            }
        }
    }
}