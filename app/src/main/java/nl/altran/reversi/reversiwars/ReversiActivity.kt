package nl.altran.reversi.reversiwars

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import nl.altran.reversi.reversiwars.databinding.ActivityReversiBinding
import nl.altran.reversi.reversiwars.di.DaggerReversiComponent
import nl.altran.reversi.reversiwars.di.RemoteModule
import nl.altran.reversi.reversiwars.di.ReversiModule
import nl.altran.reversi.reversiwars.game.scoreboard.ScoreBoard
import nl.altran.reversi.reversiwars.select_players.usecases.CreateMatchUsecase

class ReversiActivity : AppCompatActivity(), CreateMatchUsecase.Callback, PlayPauseCallback {

    private lateinit var binding: ActivityReversiBinding

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reversi)

        setupViewModel()
        setSupportActionBar(findViewById(R.id.toolbar))
    }

    private fun setupViewModel() {
        val reversiComponent = DaggerReversiComponent
                .builder()
                .remoteModule(RemoteModule())
                .reversiModule(ReversiModule(this))
                .build()

        val vm = reversiComponent.viewmodel()
        vm.storeMoveUsecase = reversiComponent.storeUsecase()

        binding.reversi = vm
        binding.swapper = reversiComponent.swapper()
    }

    override fun onGameCreated(scoreBoard: ScoreBoard) {
        binding.swapper?.isPlaying?.set(true)
        binding.reversi?.start(scoreBoard)
    }

    override fun onCreateFailed(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun onPauseGame() {
        binding.swapper?.isPlaying?.set(false)
    }

    override fun onResumeGame() {
        binding.swapper?.isPlaying?.set(true)
    }

    override fun onRestartGame(scoreBoard: ScoreBoard) {
        onGameCreated(scoreBoard)
    }

    override fun onClear() {
        binding.reversi?.scoreBoard?.get()?.winner?.set(null)
    }
}
