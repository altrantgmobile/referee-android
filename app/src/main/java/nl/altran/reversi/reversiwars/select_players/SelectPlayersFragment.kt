package nl.altran.reversi.reversiwars.select_players

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.animation.AccelerateDecelerateInterpolator
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator
import nl.altran.reversi.reversiwars.R
import nl.altran.reversi.reversiwars.databinding.FragmentSelectPlayersBinding
import nl.altran.reversi.reversiwars.game.ui.ReversiViewModel

class SelectPlayersFragment : Fragment() {

    private lateinit var binding: FragmentSelectPlayersBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_players, container, false)

        binding.viewModel = ViewModelProviders.of(this, SelectPlayersViewModelFactory(this)).get(SelectPlayersViewModel::class.java)
        binding.reversi = ViewModelProviders.of(activity!!).get(ReversiViewModel::class.java)

        setupAdapter(binding.playersList)

        setRecyclerViewHeight(binding.playersList)
        return binding.root
    }

    private fun setupAdapter(playersList: RecyclerView) {
        playersList.setHasFixedSize(true)
        playersList.itemAnimator = SlideInUpAnimator(AccelerateDecelerateInterpolator())
    }

    private fun setRecyclerViewHeight(playersList: RecyclerView) {
        playersList.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                playersList.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val lp = playersList.layoutParams
                lp.height = binding.root.height - binding.selectPlayer.bottom
                playersList.layoutParams = lp
            }
        })

        playersList.setHasFixedSize(true)
        playersList.itemAnimator = DefaultItemAnimator()
    }
}

