package nl.altran.reversi.reversiwars.select_players.usecases

import nl.altran.reversi.reversiwars.game.usecases.MatchNode
import nl.altran.reversi.reversiwars.model.Player

interface MatchRepository {
    fun createMatch(matchNode: MatchNode,
                    onSuccess: (match: String, white: Player, black: Player) -> Unit,
                    onFail: (msg: String) -> Unit)

    fun storeMove(matchId: String, moveData: MoveData)
}