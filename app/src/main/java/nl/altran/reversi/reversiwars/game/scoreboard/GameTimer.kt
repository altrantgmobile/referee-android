package nl.altran.reversi.reversiwars.game.scoreboard

import nl.altran.reversi.reversiwars.game.GameSettings
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledFuture

class GameTimer(private val settings: GameSettings = GameSettings(), private val bg: ScheduledExecutorService = Executors.newScheduledThreadPool(2)) : Runnable, AnkoLogger {

    private var future: ScheduledFuture<*>? = null
    private var callback: GameTimerCallback? = null

    fun start(gameTimerCallback: GameTimerCallback) {
        info("GameTimer.start()")
        stop()
        callback = gameTimerCallback
        future = bg.schedule(this, settings.timeout, settings.unit)
    }

    fun stop() {
        info("GameTimer.stop()")
        future?.cancel(true)
        future = null
        callback = null
    }

    override fun run() {
        info("GameTimer.onTimedOut()")
        callback?.onTimedOut()
        stop()
    }
}