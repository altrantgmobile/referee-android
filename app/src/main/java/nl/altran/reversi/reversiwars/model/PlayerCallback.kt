package nl.altran.reversi.reversiwars.model

import nl.altran.reversi.data.Move

interface PlayerCallback {
    var currentPlayer: Player
    fun onMoveSubmitted(player: Player, move: Move)
}