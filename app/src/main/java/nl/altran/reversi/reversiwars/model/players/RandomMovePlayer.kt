package nl.altran.reversi.reversiwars.model.players

import nl.altran.reversi.reversiwars.model.util.BoardUtil

class RandomMovePlayer : BasePlayer() {

    override fun name(): String {
        return "Random Move"
    }

    override fun human(): Boolean {
        return false
    }

    override fun local(): Boolean {
        return true
    }

    override fun onYourTurn(board: String) {
        val moves = BoardUtil.findPossibleMoves(board, stoneColor)
        submitMove(moves[(Math.random() * moves.size).toInt()])
    }

    override fun onRejected(board: String) {
        onYourTurn(board)
    }
}