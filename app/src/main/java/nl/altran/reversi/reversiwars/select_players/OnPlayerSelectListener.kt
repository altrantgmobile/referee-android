package nl.altran.reversi.reversiwars.select_players

import nl.altran.reversi.reversiwars.model.Player

interface OnPlayerSelectListener {
    fun onPlayerSelected(player: Player)
}