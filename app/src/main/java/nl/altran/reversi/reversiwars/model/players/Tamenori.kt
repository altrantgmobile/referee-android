package nl.altran.reversi.reversiwars.model.players

import android.support.annotation.VisibleForTesting
import nl.altran.reversi.data.Move
import nl.altran.reversi.reversiwars.model.Stone
import nl.altran.reversi.reversiwars.model.board.Board
import nl.altran.reversi.reversiwars.model.util.BoardUtil

class Tamenori(val maxDepth: Int) : BasePlayer() {

    override fun name(): String {
        return when (stoneColor) {
            Stone.Color.EMPTY -> "Tamenori $maxDepth"
            else -> "Tamenori $maxDepth"
        }
    }

    override fun human(): Boolean {
        return false
    }

    override fun local(): Boolean {
        return true
    }

    override fun onRejected(board: String) {
        onYourTurn(board)
    }

    override fun onYourTurn(board: String) {
        val gameBoard = BoardUtil.extractBoard(board)
        val moveList = BoardUtil.findPossibleMoves(gameBoard, stoneColor)
        val miniMax = minimax(gameBoard, moveList, maxDepth, Integer.MIN_VALUE, Integer.MAX_VALUE, stoneColor.value, moveList[0])
        submitMove(miniMax.first)
    }

    private fun minimax(board: Board, moves: List<Move>, depth: Int, alpha: Int, beta: Int, color: Int, move: Move): Pair<Move, Int> {
        var bestMove = move
        var newAlpha = alpha
        var newBeta = beta

        if (depth == 0 || moves.isEmpty()) {
            return Pair(bestMove, eval(board, color))
        }

        if (color == stoneColor.value) {
            // MAX
            var score = Integer.MIN_VALUE
            for (it in moves) {
                val child = board.clone()
                child.applyMove(it, color)
                val maxi = minimax(child, BoardUtil.findPossibleMoves(child, stoneColor.flip()), depth - 1, newAlpha, newBeta, -1 * color, it)
                if (maxi.second > score) {
                    bestMove = it
                    score = Math.max(score, maxi.second)
                }
                newAlpha = Math.max(alpha, score)
                if (newBeta <= newAlpha) break
            }

            return Pair(bestMove, score)

        } else {
            // MIN
            var score = Integer.MAX_VALUE
            for (it in moves) {
                val child = board.clone().apply { applyMove(it, color) }
                val flippedColor = -1 * color
                val mini = minimax(child, BoardUtil.findPossibleMoves(child, flippedColor), depth - 1, newAlpha, newBeta, flippedColor, it)
                if (mini.second < score) {
                    bestMove = it
                    score = Math.min(score, mini.second)
                }
                newBeta = Math.min(newBeta, score)
                if (newBeta <= newAlpha) break
            }

            return Pair(bestMove, score)
        }
    }

    private fun eval(board: Board, color: Int): Int {

        val corners = 1000 * evalCorners(board)
        val safe = 100 * evalSafeStones(board)
        val stones = evalScore(board)
        val opponentMoves = 10 * BoardUtil.findPossibleMoves(board, -1 * color).size
        val eval = corners + safe + stones - opponentMoves
        return stoneColor.value * eval
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    internal fun evalCorners(board: Board): Int {
        val size = board.size - 1
        return arrayOf(0, size, size * size + 1, size * size + size).sumBy { board.get(it).color.get()!!.value }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun evalSafeStones(board: Board): Int {
        val safeStones = board.stones
                .filter {
                    it.color.get() != Stone.Color.EMPTY
                }
                .filter {
                    isSafe(board, it)
                }
                .sumBy {
                    it.color.get()!!.value
                }
        return safeStones
    }

    private fun isSafe(board: Board, stone: Stone): Boolean {
        val hor = isSafeHorizontal(board, stone)
        val ver = isSafeVertical(board, stone)
        val dia = isSafeDiagonal(board, stone)
        val aid = isSafeLanogaid(board, stone)
        return hor && ver && dia && aid
    }

    private fun check(board: Board, stone: Stone, row: Int, col: Int) = board.get(stone.row + row, stone.col + col).color.get()

    private fun checkRow(range: IntRange, board: Board, stone: Stone): Boolean {
        return range.none {
            val color = board.get(stone.row, it).color.get()
            color == Stone.Color.EMPTY || color != stone.color.get()
        }
    }

    private fun checkCol(range: IntRange, board: Board, stone: Stone): Boolean {
        return range.none {
            val color = board.get(it, stone.col).color.get()
            color == Stone.Color.EMPTY || color != stone.color.get()
        }
    }

    private fun isSafeHorizontal(board: Board, stone: Stone): Boolean {
        if (stone.col - 1 < 0 || stone.col + 1 > board.size - 1) return true
        val left = check(board, stone, 0, -1) == stone.color.get() && checkRow(0 until stone.col, board, stone)
        val right = check(board, stone, 0, 1) == stone.color.get() && checkRow(stone.col + 1 until board.size, board, stone)
        if (left || right) return true
        return false
    }

    private fun isSafeVertical(board: Board, stone: Stone): Boolean {
        if (stone.row - 1 < 0 || stone.row + 1 > board.size - 1) return true
        val top = check(board, stone, -1, 0) == stone.color.get() && checkCol(0 until stone.row, board, stone)
        val bottom = check(board, stone, 1, 0) == stone.color.get() && checkCol(stone.col + 1 until board.size, board, stone)
        if (top || bottom) return true
        return false
    }

    private fun isSafeDiagonal(board: Board, stone: Stone): Boolean {
        if (stone.row - 1 < 0 || stone.col - 1 < 0 || stone.row + 1 > board.size - 1 || stone.col + 1 > board.size - 1) return true
        val left = check(board, stone, -1, -1) == stone.color.get()
        val right = check(board, stone, 1, 1) == stone.color.get()
        if (left || right) return true
        return false
    }

    private fun isSafeLanogaid(board: Board, stone: Stone): Boolean {
        if (stone.row - 1 < 0 || stone.col - 1 < 0 || stone.row + 1 > board.size - 1 || stone.col + 1 > board.size - 1) return true
        val left = check(board, stone, -1, 1) == stone.color.get()
        val right = check(board, stone, 1, -1) == stone.color.get()
        if (left || right) return true
        return false
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun evalScore(board: Board): Int {
        return board.stones.sumBy { board.get(it.row, it.col).color.get()!!.value }
    }
}