package nl.altran.reversi.reversiwars

import android.view.View
import android.view.animation.AnimationUtils


object AnimationHelper {
    fun shake(view: View) {
        val animShake = AnimationUtils.loadAnimation(view.context, R.anim.shake)
        view.startAnimation(animShake)
    }

    fun flyOut(view: View, perc: Float) {
        view.alpha = perc
        view.scaleX = perc
        view.scaleY = perc
    }

    fun fadeIn(view: View, perc: Float) {
        view.scaleX = perc
        view.scaleY = perc
        view.alpha = perc
    }
}