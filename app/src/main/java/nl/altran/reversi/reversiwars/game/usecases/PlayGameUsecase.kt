package nl.altran.reversi.reversiwars.game.usecases

import nl.altran.reversi.data.Move
import nl.altran.reversi.reversiwars.model.Stone
import javax.inject.Inject

class PlayGameUsecase @Inject constructor(private val repo: GameRepository) {

    companion object {
        val POINTS_WIN = 200
        val POINTS_DRAW = 100
        val POINTS_LOSS = 0
    }

    fun joinGame(matchUuid: String, stoneColor: Stone.Color) {
        repo.joinGame(matchUuid, stoneColor)
    }

    fun gameFinished(yourScore: Int, opponentScore: Int) {
        val points = getPointsForScores(Math.abs(yourScore), Math.abs(opponentScore))
        repo.gameFinished(yourScore, opponentScore, points)
    }

    fun handleTurn(board: String, submit: (Move) -> Unit) {
        repo.handleTurn(board, submit)
    }

    fun handleRejected() {
        repo.reject()
    }

    private fun getPointsForScores(yourScore: Int, opponentScore: Int): Int {
        return if (yourScore > opponentScore) POINTS_WIN else if (yourScore == opponentScore) POINTS_DRAW else POINTS_LOSS
    }
}