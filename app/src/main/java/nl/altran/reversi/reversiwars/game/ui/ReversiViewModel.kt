package nl.altran.reversi.reversiwars.game.ui

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableField
import android.support.design.widget.Snackbar
import android.view.View
import me.tatarka.bindingcollectionadapter2.ItemBinding
import me.tatarka.bindingcollectionadapter2.itembindings.OnItemBindModel
import nl.altran.reversi.data.Move
import nl.altran.reversi.reversiwars.BR
import nl.altran.reversi.reversiwars.PlayPauseCallback
import nl.altran.reversi.reversiwars.R
import nl.altran.reversi.reversiwars.game.scoreboard.ScoreBoard
import nl.altran.reversi.reversiwars.model.Player
import nl.altran.reversi.reversiwars.model.Referee
import nl.altran.reversi.reversiwars.model.RefereeCallback
import nl.altran.reversi.reversiwars.model.Stone
import nl.altran.reversi.reversiwars.model.board.Board
import nl.altran.reversi.reversiwars.model.board.ObservableBoard
import nl.altran.reversi.reversiwars.select_players.usecases.StoreMoveUsecase
import org.jetbrains.anko.AnkoLogger
import javax.inject.Inject

class ReversiViewModel @Inject constructor() : ViewModel(), StoneClickListener, RefereeCallback, AnkoLogger {

    var playPauseCallback: PlayPauseCallback? = null
    val board: ObservableBoard = ObservableBoard(Board())
    val stoneBinding: OnItemBindModel<Stone> = OnItemBindModel()
    val stone: ItemBinding<Stone> = ItemBinding.of(stoneBinding).bindExtra(BR.stoneClickListener, this)
    val scoreBoard: ObservableField<ScoreBoard> = ObservableField()
    val referee: Referee = Referee(Board())

    var storeMoveUsecase: StoreMoveUsecase? = null

    fun start(_scoreBoard: ScoreBoard) {
        scoreBoard.set(_scoreBoard.onStart())
        board.restart()
        referee.start(_scoreBoard.matchId, _scoreBoard.players(), this)
    }

    fun onResumeMatch(view: View) {
        when {
            gameEnded() -> playPauseCallback?.onRestartGame(scoreBoard())
            gameWasPaused() -> playPauseCallback?.onResumeGame()
            else -> showMessage(view)
        }
    }

    private fun gameEnded(): Boolean {
        return scoreBoard.get() != null && scoreBoard().winner.get() != null
    }

    private fun gameWasPaused(): Boolean {
        return scoreBoard.get() != null && scoreBoard().winner.get() == null
    }

    fun onPauseMatch() {
        playPauseCallback?.onPauseGame()
    }

    private fun showMessage(view: View) {
        Snackbar.make(view.rootView,
                R.string.select_players_first,
                Snackbar.LENGTH_INDEFINITE).setAction(android.R.string.ok, {}).show()
    }

    override fun onStoneClicked(stone: Stone) {
        val currentPlayer = scoreBoard().currentPlayer
        if (currentPlayer.human()) {
            referee.onMoveSubmitted(currentPlayer, Move(stone.row, stone.col))
        }
    }

    override fun rejectMove(player: Player, move: Move) {
        scoreBoard().onMoveRejected(player)
    }

    override fun timedOut() {
        next()
    }

    override fun submitMove(player: Player, move: Move, list: List<Stone>) {
        updateBoard(player, list, move)
        storeMove(player, move)
        next()
    }

    private fun storeMove(player: Player, move: Move) {
        storeMoveUsecase?.go(scoreBoard.get()!!.matchId, player, move)
    }

    private fun updateBoard(player: Player, list: List<Stone>, move: Move) {
        scoreBoard().onMoveSubmitted(player, list.size)
        board.performMove(move, player.color(), list)
    }

    private fun next() {
        referee.toggle()
        scoreBoard().currentPlayer = referee.currentPlayer
    }

    override fun gameFinished(white: Player, scoreWhite: Int, black: Player, scoreBlack: Int) {
        scoreBoard().onGameFinished(white, scoreWhite, black, scoreBlack)
    }

    private fun scoreBoard() = scoreBoard.get()!!
}