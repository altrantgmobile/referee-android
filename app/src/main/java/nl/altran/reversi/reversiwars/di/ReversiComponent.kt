package nl.altran.reversi.reversiwars.di

import dagger.Component
import nl.altran.reversi.reversiwars.FragmentSwapper
import nl.altran.reversi.reversiwars.game.ui.ReversiViewModel
import nl.altran.reversi.reversiwars.select_players.usecases.StoreMoveUsecase

@Component(modules = arrayOf(ReversiModule::class, RemoteModule::class))
interface ReversiComponent {
    fun viewmodel(): ReversiViewModel
    fun swapper(): FragmentSwapper
    fun storeUsecase(): StoreMoveUsecase
}