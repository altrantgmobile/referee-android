package nl.altran.reversi.reversiwars.game.scoreboard

interface GameTimerCallback {
    fun onTimedOut()
}