package nl.altran.reversi.reversiwars.game.usecases.firebase

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.gson.GsonBuilder
import nl.altran.reversi.data.PlayerNode
import nl.altran.reversi.data.ReversiResultData
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class FirebaseStatsWriter(val result: ReversiResultData) : ValueEventListener {

    private val gson = GsonBuilder().create()
    private val logger = AnkoLogger<FirebaseStatsWriter>()

    override fun onDataChange(p0: DataSnapshot?) {
        logger.info("onDataChange: $p0")
        logger.info("onDataChange.ref: ${p0?.ref}")
        logger.info("onDataChange.vale: ${p0?.value}")
        if (p0 != null && p0.value != null) {
            onDataChange(p0.value.toString(),
                    { p0.ref.setValue( it.update(result) ) }
            )
        } else if(p0 != null){
            p0.ref.setValue(PlayerNode.Stats(result.points, result.win, result.drew, result.lost))
        }
    }

    override fun onCancelled(p0: DatabaseError?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun onDataChange(json: String, result: (PlayerNode.Stats) -> Unit) {
        if (json != "") {
            val stats = parse(json)
            if (stats != null) {
                result.invoke(stats)
            }
        }
    }

    private fun parse(json: String): PlayerNode.Stats? {
        return try {
            gson.fromJson(json, PlayerNode.Stats::class.java)
        } catch (invalid: Throwable) {
            null
        }
    }
}