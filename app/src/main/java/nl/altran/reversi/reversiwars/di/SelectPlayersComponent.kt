package nl.altran.reversi.reversiwars.di

import dagger.Component
import nl.altran.reversi.reversiwars.select_players.SelectPlayersViewModel

@Component(modules = arrayOf(SelectPlayersModule::class, RemoteModule::class))
interface SelectPlayersComponent {
    fun viewmodel(): SelectPlayersViewModel
}
