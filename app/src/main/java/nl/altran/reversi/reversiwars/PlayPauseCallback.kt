package nl.altran.reversi.reversiwars

import nl.altran.reversi.reversiwars.game.scoreboard.ScoreBoard

interface PlayPauseCallback {
    fun onPauseGame()
    fun onResumeGame()
    fun onRestartGame(scoreBoard: ScoreBoard)
}