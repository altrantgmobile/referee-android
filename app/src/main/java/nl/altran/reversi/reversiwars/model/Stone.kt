package nl.altran.reversi.reversiwars.model

import android.databinding.BindingAdapter
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.widget.ImageView
import me.tatarka.bindingcollectionadapter2.ItemBinding
import me.tatarka.bindingcollectionadapter2.itembindings.ItemBindingModel
import nl.altran.reversi.reversiwars.BR
import nl.altran.reversi.reversiwars.R
import org.jetbrains.anko.AnkoLogger

data class Stone constructor(val row: Int,
                             val col: Int,
                             val color: ObservableField<Color> = ObservableField(Stone.Color.EMPTY))
    : ItemBindingModel, AnkoLogger {

    override fun onItemBind(itemBinding: ItemBinding<*>?) {
        itemBinding!!.set(BR.stone, R.layout.item_stone)
    }

    val last = ObservableBoolean(false)

    enum class Color(val value: Int) {
        EMPTY(0),
        BLACK(-1),
        WHITE(1);

        fun asInt(): Int {
            return value
        }

        fun flip(): Stone.Color {
            return when (value) {
                0 -> Stone.Color.EMPTY
                1 -> Stone.Color.BLACK
                else -> Stone.Color.WHITE
            }
        }

        companion object {
            fun fromInt(value: Int): Stone.Color {
                return when (value) {
                    0 -> Stone.Color.EMPTY
                    1 -> Stone.Color.WHITE
                    else -> Stone.Color.BLACK
                }
            }
        }
    }

    fun flip(): Stone {
        when (color.get()) {
            Color.BLACK -> color.set(Color.WHITE)
            Color.WHITE -> color.set(Color.BLACK)
            else -> {
                color.set(Color.EMPTY)
            }
        }
        return this
    }

    fun last(isLast: Boolean): Stone {
        last.set(isLast)
        return this
    }

    fun set(color: Color) {
        this.color.set(color)
    }

    companion object StoneBinding {
        @JvmStatic
        @BindingAdapter("stone")
        fun doAppearance(view: ImageView, new: Stone.Color?) {
            if (new != null && new != Color.EMPTY) {
                view.setImageResource(R.drawable.stone)
                view.isSelected = new == Color.BLACK
            } else {
                view.setImageDrawable(null)
            }
        }

        @JvmStatic
        @BindingAdapter("last")
        fun doAppearance(view: ImageView, last: Boolean) {
            view.isSelected = last
        }
    }
}