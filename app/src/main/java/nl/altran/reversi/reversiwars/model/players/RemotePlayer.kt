package nl.altran.reversi.reversiwars.model.players

import nl.altran.reversi.reversiwars.game.usecases.PlayGameUsecase

class RemotePlayer(private val name: String, private val gameUsecase: PlayGameUsecase, playerUuid: String) : BasePlayer(guid = playerUuid) {

    override fun name(): String {
        return name
    }

    override fun human(): Boolean {
        return false
    }

    override fun local(): Boolean {
        return false
    }

    override fun onJoinedGame(guid: String) {
        gameUsecase.joinGame(guid, stoneColor)
    }

    override fun onGameFinished(yourScore: Int, opponentScore: Int) {
        gameUsecase.gameFinished(yourScore, opponentScore)
    }

    override fun onYourTurn(board: String) {
        gameUsecase.handleTurn(
                board,
                { submitMove(it) }
        )
    }

    override fun onRejected(board: String) {
        gameUsecase.handleRejected()
        onYourTurn(board)
    }
}