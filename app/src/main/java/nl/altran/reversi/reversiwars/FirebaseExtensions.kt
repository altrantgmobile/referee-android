package nl.altran.reversi.reversiwars

import android.os.Handler
import android.os.Looper
import com.google.firebase.database.DataSnapshot
import nl.altran.reversi.data.PublicNode
import nl.altran.reversi.reversiwars.game.usecases.PlayGameUsecase
import nl.altran.reversi.reversiwars.game.usecases.firebase.FirebaseGameRepository
import nl.altran.reversi.reversiwars.model.Player
import nl.altran.reversi.reversiwars.model.players.RemotePlayer
import java.util.concurrent.Executors

private val executor = Executors.newFixedThreadPool(4)
private val handle = Handler(Looper.getMainLooper())

fun DataSnapshot.toRemotePlayer(done: (Player) -> Unit) {
    executor.submit({
        val usecase = PlayGameUsecase(FirebaseGameRepository(ref))
        val player = toRemotePlayer(this, key, usecase)
        if (player != null) {
            handle.post({
                done(player)
            })
        }
    })
}

private fun toRemotePlayer(data: DataSnapshot, uuid: String, usecase: PlayGameUsecase): RemotePlayer? {
    val player: PublicNode = try {
        data.getValue(PublicNode::class.java)!!
    } catch (err: Throwable) {
        return null
    }

    val playerName = player.public.name
    return RemotePlayer(playerName, usecase, uuid)
}