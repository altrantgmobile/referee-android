package nl.altran.reversi.reversiwars.game.ui

import nl.altran.reversi.reversiwars.model.Stone

interface StoneClickListener {
    fun onStoneClicked(stone: Stone)
}