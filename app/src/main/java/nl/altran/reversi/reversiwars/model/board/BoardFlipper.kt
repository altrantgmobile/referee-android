package nl.altran.reversi.reversiwars.model.board

import nl.altran.reversi.data.Move
import nl.altran.reversi.reversiwars.model.Stone

class BoardFlipper(val board: Board, val size: Int = board.size) {

    fun flip(move: Move, stone: Stone.Color) : List<Stone> {
        val positions = emptyList<Stone>().toMutableList()
        positions.addAll(flipLeft(move, stone))
        positions.addAll(flipRight(move, stone))
        positions.addAll(flipUp(move, stone))
        positions.addAll(flipDown(move, stone))
        positions.addAll(flipNW(move, stone))
        positions.addAll(flipNE(move, stone))
        positions.addAll(flipSE(move, stone))
        positions.addAll(flipSW(move, stone))
        return positions
    }

    private fun check(row: Int, col: Int, stone: Stone.Color, positions: MutableList<Stone>): Boolean {
        if (row < 0 || row >= size || col < 0 || col >= size) {
            positions.clear()
            return true
        }

        val other = board.get(row, col)
        when {
            other.color.get() === Stone.Color.EMPTY -> {
                positions.clear()
                return true
            }
            other.color.get() == stone -> return true
            else -> positions.add(other)
        }
        return false
    }

    private fun flipMoves(stones: List<Stone>): List<Stone> {
        for (stone in stones) {
            stone.flip()
            board.set(stone, stone.color.get()!!)
        }
        return stones
    }

    private fun flipLeft(move: Move, stone: Stone.Color): List<Stone> {
        val positions = emptyList<Stone>().toMutableList()
        for (col in move.col - 1 downTo -1) {
            if (check(move.row, col, stone, positions)) {
                break
            }
        }

        return flipMoves(positions)
    }

    private fun flipRight(move: Move, stone: Stone.Color): List<Stone> {
        val positions = emptyList<Stone>().toMutableList()

        for (col in move.col + 1..size) {
            if (check(move.row, col, stone, positions)) {
                break
            }
        }

        return flipMoves(positions)
    }

    private fun flipUp(move: Move, stone: Stone.Color): List<Stone> {
        val positions = emptyList<Stone>().toMutableList()

        for (row in move.row - 1 downTo -1) {
            if (check(row, move.col, stone, positions)) {
                break
            }
        }

        return flipMoves(positions)
    }

    private fun flipDown(move: Move, stone: Stone.Color): List<Stone> {
        val positions = emptyList<Stone>().toMutableList()

        for (row in move.row + 1..size) {
            if (check(row, move.col, stone, positions)) {
                break
            }
        }
        return flipMoves(positions)
    }

    private fun flipNW(move: Move, stone: Stone.Color): List<Stone> {
        val positions = emptyList<Stone>().toMutableList()

        for (x in 1..size) {
            if (check(move.row + x, move.col - x, stone, positions)) {
                break
            }
        }
        return flipMoves(positions)
    }

    private fun flipNE(move: Move, stone: Stone.Color): List<Stone> {
        val positions = emptyList<Stone>().toMutableList()

        for (x in 1..size) {
            if (check(move.row + x, move.col + x, stone, positions)) {
                break
            }
        }
        return flipMoves(positions)
    }

    private fun flipSE(move: Move, stone: Stone.Color): List<Stone> {
        val positions = emptyList<Stone>().toMutableList()

        for (x in 1..size) {
            if (check(move.row - x, move.col - x, stone, positions)) {
                break
            }
        }
        return flipMoves(positions)
    }

    private fun flipSW(move: Move, stone: Stone.Color): List<Stone> {
        val positions = emptyList<Stone>().toMutableList()

        for (x in 1..size) {
            if (check(move.row - x, move.col + x, stone, positions)) {
                break
            }
        }
        return flipMoves(positions)
    }
}