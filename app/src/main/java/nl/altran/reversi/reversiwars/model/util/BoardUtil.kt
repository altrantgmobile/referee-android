package nl.altran.reversi.reversiwars.model.util

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import nl.altran.reversi.data.BoardData
import nl.altran.reversi.data.Move
import nl.altran.reversi.reversiwars.model.Stone
import nl.altran.reversi.reversiwars.model.board.Board
import java.io.File
import java.io.FileInputStream
import java.io.IOException


class BoardUtil {
    companion object {

        private val gson : Gson = GsonBuilder().create()

        @Throws(IOException::class)
        private fun getStringFromFile(filePath: String): String {
            val fl = File(filePath)
            val fin = FileInputStream(fl)
            val inputAsString = fin.bufferedReader().use { it.readText() }
            //Make sure you close all streams.
            fin.close()
            return inputAsString
        }

        fun extractBoardFromFile(file: String): Board {
            val resource = BoardUtil::class.java.classLoader.getResource(file)
            return extractBoard(getStringFromFile(resource.path))
        }

        fun extractBoard(board: String): Board {
            val parsed = gson.fromJson(board, BoardData::class.java) ?: throw IllegalArgumentException("invalid data $board")
            return toBoard(parsed)
        }

        fun toBoard(data: BoardData): Board {
            val board = Board(data.size)
            return board.setupWithData(data.raw)
        }

        fun findPossibleMoves(board: String, stoneColor: Stone.Color): List<Move> {
            return findPossibleMoves(extractBoard(board), stoneColor)
        }

        fun findPossibleMoves(board: Board, stoneColor: Stone.Color): List<Move> {
            return findPossibleMoves(board, stoneColor.value)
        }
        fun findPossibleMoves(board: Board, stoneColor: Int): List<Move> {
            val possibleMoves = emptyList<Move>().toMutableList()
            // 1) Find empty stones
            board.stones.filter { it.color.get() == Stone.Color.EMPTY }.forEach {
                val move = Move(it.row, it.col)
                if (board.clone().applyMove(move, stoneColor).isNotEmpty()) {
                    possibleMoves.add(move)
                }
            }

            return possibleMoves
        }
    }
}