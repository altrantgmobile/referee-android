package nl.altran.reversi.reversiwars.select_players.usecases.firebase

import com.google.firebase.database.*
import nl.altran.reversi.reversiwars.model.Player
import nl.altran.reversi.reversiwars.select_players.usecases.FetchPlayersUsecase
import nl.altran.reversi.reversiwars.select_players.usecases.PingUsecase
import nl.altran.reversi.reversiwars.select_players.usecases.PlayersRepository
import nl.altran.reversi.reversiwars.toRemotePlayer
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class FirebasePlayersRepository(database: FirebaseDatabase) : PlayersRepository, ValueEventListener, ChildEventListener {
    private var logger = AnkoLogger<FirebasePlayersRepository>()
    private val playerRef: DatabaseReference = database.getReference("players")
    private lateinit var callback: FetchPlayersUsecase.Callback

    override fun fetchRemotePlayers(callback: FetchPlayersUsecase.Callback) {
        this.callback = callback
        listenForPlayers()
    }

    private fun listenForPlayers() {
        this.playerRef.addValueEventListener(this)
        this.playerRef.addChildEventListener(this)
    }

    override fun onDataChange(p0: DataSnapshot?) {
        p0?.toRemotePlayer {
            ping(it, { callback.onPlayerRetrieved(it) })
        }
    }

    override fun onChildAdded(p0: DataSnapshot, p1: String?) {
        p0.toRemotePlayer {
            ping(it) { callback.onPlayerRetrieved(it) }
        }
    }

    private fun ping(player: Player, function: (Player) -> Unit) {
        PingUsecase(playerRef).ping(player, function)
    }

    override fun onChildRemoved(p0: DataSnapshot) {
        p0.toRemotePlayer { callback.onPlayerRemoved(it.name()) }
    }

    override fun onChildChanged(p0: DataSnapshot, p1: String?) {
        p0.toRemotePlayer { callback.onPlayerUpdated(it) }
    }

    override fun onChildMoved(p0: DataSnapshot, p1: String?) {
        logger.info("onChildMoved $p0")
    }

    override fun onCancelled(p0: DatabaseError) {
        logger.info("onCancelled $p0")
    }
}