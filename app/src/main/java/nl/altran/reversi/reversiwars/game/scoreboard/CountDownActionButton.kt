package nl.altran.reversi.reversiwars.game.scoreboard

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.os.CountDownTimer
import android.support.design.widget.FloatingActionButton
import android.util.AttributeSet
import nl.altran.reversi.reversiwars.game.GameSettings
import nl.altran.reversi.reversiwars.model.Stone
import org.jetbrains.anko.AnkoLogger

class CountDownActionButton : FloatingActionButton, AnkoLogger {
    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val settings = GameSettings()
    private val thickness = 6F
    private var oval: RectF = RectF(0F, 0F, 0F, 0F)
    private val paint: Paint = Paint()

    val countDown = settings.unit.toMillis(settings.timeout)
    var elapsed = 0F

    private var timer = object : CountDownTimer(countDown, 1000 / 60) {
        override fun onTick(millisUntilFinished: Long) {
            elapsed = ((countDown - millisUntilFinished) * 1.0F / countDown) * 360.0F
            invalidate()
        }

        override fun onFinish() {
            cancelCountDown()
        }
    }

    init {
        paint.color = Color.WHITE
        paint.style = Paint.Style.STROKE
        paint.strokeCap = Paint.Cap.ROUND
        paint.strokeWidth = thickness
        paint.isAntiAlias = true
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        oval = RectF(0F + thickness / 2, 0F + thickness / 2, w.toFloat() - thickness / 2, h.toFloat() - thickness / 2)
    }

    fun startCountDown(color: Stone.Color) {
        // Start Countdown && fade in
        paint.color = if (color == Stone.Color.WHITE) Color.WHITE else Color.BLACK
        paint.alpha = if (color == Stone.Color.WHITE) 255 else 128
        elapsed = 0F
        timer.start()
        invalidate()
    }

    fun cancelCountDown() {
        // Stop Countdown && fade out
        elapsed = 360F + 10F
        timer.cancel()
        invalidate()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (elapsed <= 360F) {
            val sweepAngle = 360F - elapsed
            canvas?.drawArc(oval, -90F, sweepAngle, false, paint)
        }
    }

}