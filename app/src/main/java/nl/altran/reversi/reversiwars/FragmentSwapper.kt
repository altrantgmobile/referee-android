package nl.altran.reversi.reversiwars

import android.databinding.ObservableBoolean
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.transition.ChangeImageTransform
import android.transition.Slide
import android.transition.TransitionSet
import android.view.Gravity
import android.view.View
import nl.altran.reversi.reversiwars.game.scoreboard.ScoreFragment
import nl.altran.reversi.reversiwars.select_players.SelectPlayersFragment

class FragmentSwapper(private val fragManager: FragmentManager, transitionSet: TransitionSet = TransitionSet()) {

    val isPlaying = ObservableBoolean(false)

    private val scoreEnter = transitionSet.clone()
    private val scoreExit = transitionSet.clone()
    private val selectEnter = transitionSet.clone()
    private val selectExit = transitionSet.clone()


    init {
        scoreEnter.addTransition(Slide(Gravity.BOTTOM)).excludeTarget(R.id.black, true).excludeTarget(R.id.black_name, true).excludeTarget(R.id.white, true).excludeTarget(R.id.white_name, true)
        scoreExit.addTransition(Slide(Gravity.TOP)).excludeTarget(R.id.black, true).excludeTarget(R.id.black_name, true).excludeTarget(R.id.white, true).excludeTarget(R.id.white_name, true)
        selectEnter.addTransition(Slide()).excludeTarget(R.id.black, true).excludeTarget(R.id.black_name, true).excludeTarget(R.id.white, true).excludeTarget(R.id.white_name, true)
        selectExit.addTransition(Slide()).excludeTarget(R.id.black, true).excludeTarget(R.id.black_name, true).excludeTarget(R.id.white, true).excludeTarget(R.id.white_name, true)
    }

    fun swap(isPlaying: Boolean) {
        if (isPlaying) {
            showFragment(ScoreFragment())
        } else {
            showFragment(SelectPlayersFragment())
        }
    }

    fun onToggleAndSlide(state: Int, perc: Float) {
        if (state != BottomSheetToggler.SLIDING) {
            isPlaying.set(state == BottomSheetBehavior.STATE_COLLAPSED)
        }

        val current = fragManager.findFragmentById(R.id.container)
        if (current is SlidableFragment) {
            current.onSlide(perc)
        }
    }

    private fun showFragment(toShow: Fragment) {

        toShow.enterTransition = if (toShow is ScoreFragment) scoreEnter else selectEnter
        toShow.exitTransition = if (toShow is ScoreFragment) scoreExit else selectExit
        toShow.sharedElementEnterTransition = ChangeImageTransform()
        toShow.sharedElementReturnTransition = ChangeImageTransform()

        val beginTransaction = fragManager.beginTransaction()

        findShared()
                .filterNotNull()
                .forEach { beginTransaction.addSharedElement(it, it.transitionName) }

        beginTransaction
                .replace(R.id.container, toShow, "Swap")
                .commit()
    }

    private fun findShared(): Array<View?> {
        val frag = fragManager.findFragmentByTag("Swap")
        return Array(1, { frag?.view?.findViewById<FloatingActionButton>(R.id.fab) })
    }
}