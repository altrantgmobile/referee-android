package nl.altran.reversi.reversiwars.model.players

import nl.altran.reversi.reversiwars.model.Stone

class HumanPlayer(private val name: String = "Human") : BasePlayer() {

    override fun name(): String {
        return when(stoneColor){
            Stone.Color.EMPTY -> name
            else -> "$name $stoneColor"
        }
    }

    override fun human(): Boolean {
        return true
    }

    override fun local(): Boolean {
        return false
    }

    override fun onYourTurn(board: String) {}

    override fun onRejected(board: String) {}
}