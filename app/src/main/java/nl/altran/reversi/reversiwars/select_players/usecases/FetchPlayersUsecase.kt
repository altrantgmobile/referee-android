package nl.altran.reversi.reversiwars.select_players.usecases

import nl.altran.reversi.reversiwars.model.Player
import nl.altran.reversi.reversiwars.model.players.*
import javax.inject.Inject

class FetchPlayersUsecase @Inject constructor(private val repo: PlayersRepository) {

    interface Callback {
        fun onPlayerRetrieved(player: Player)
        fun onPlayerUpdated(player: Player)
        fun onPlayerRemoved(name: String)
    }

    var callback: Callback? = null

    fun registerCallback(callback: Callback) {
        this.callback = callback

        foundPlayer(HumanPlayer())
        foundPlayer(HumanPlayer())
        foundPlayer(RandomPlayer())
        foundPlayer(RandomMovePlayer())
        foundPlayer(BeatMe(3))
        foundPlayer(BeatMe(5))
        foundPlayer(Tamenori(3))

        repo.fetchRemotePlayers(callback)
    }

    fun unregisterCallback() {
        this.callback = null
    }

    private fun foundPlayer(player: Player) {
        this.callback?.onPlayerRetrieved(player)
    }
}

