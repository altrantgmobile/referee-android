package nl.altran.reversi.reversiwars.select_players.usecases

interface PlayersRepository {
    fun fetchRemotePlayers(callback: FetchPlayersUsecase.Callback)
}