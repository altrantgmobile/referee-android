package nl.altran.reversi.reversiwars.game.usecases.firebase

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.gson.GsonBuilder
import nl.altran.reversi.data.Move

class FirebaseMoveListener(private val submit: (Move) -> Unit, private val unsubscribe: (FirebaseMoveListener) -> Unit) : ValueEventListener {

    private val gson = GsonBuilder().create()

    override fun onDataChange(p0: DataSnapshot?) {
        if(p0 != null && p0.value != null){
            onDataChange(p0.value.toString())
        }
    }

    override fun onCancelled(p0: DatabaseError?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun onDataChange(json: String) {
        if(json != ""){
            if("rejected" != json){
                val move = parse(json)
                if (move != null) {
                    submit.invoke(move)
                    unsubscribe.invoke(this)
                }
            }
        }
    }

    private fun parse(json: String): Move? {
        return try {
            gson.fromJson(json, Move::class.java)
        } catch (invalid: Throwable){
            null
        }
    }
}