package nl.altran.reversi.reversiwars.model.players

import nl.altran.reversi.data.Move
import nl.altran.reversi.reversiwars.model.Player
import nl.altran.reversi.reversiwars.model.PlayerCallback
import nl.altran.reversi.reversiwars.model.Stone
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import java.util.*

abstract class BasePlayer(private val guid: String = UUID.randomUUID().toString()) : Player, AnkoLogger {

    private var gameCallback: PlayerCallback? = null
    var stoneColor: Stone.Color = Stone.Color.EMPTY

    final override fun guid(): String {
        return guid
    }

    final override fun playerCallback(playerCallback: PlayerCallback) {
        this.gameCallback = playerCallback
    }

    final override fun stoneColor(color: Stone.Color) {
        this.stoneColor = color
    }

    final override fun color(): Stone.Color {
        return stoneColor
    }

    final override fun yourTurn(board: String) {
        info("yourTurn\n$board")
        doSetOurTurn()
        onYourTurn(board)
    }

    final override fun onMoveRejected(board: String) {
        onRejected(board)
    }

    private fun doSetOurTurn() {
        this.gameCallback?.currentPlayer = this
    }

    override fun onJoinedGame(guid: String) {
        // needs to be implemented by subclass
    }

    override fun onGameFinished(yourScore: Int, opponentScore: Int) {
        // needs to be implemented by subclass
    }

    protected fun submitMove(move: Move) {
        info("submit to Arbiter ($move)")
        this.gameCallback?.onMoveSubmitted(this, move)
    }

    protected abstract fun onYourTurn(board: String)
    protected abstract fun onRejected(board: String)

    override fun equals(other: Any?): Boolean {
        if (other is Player) return guid() == other.guid()
        return false
    }

    override fun hashCode(): Int {
        return guid().hashCode()
    }
}