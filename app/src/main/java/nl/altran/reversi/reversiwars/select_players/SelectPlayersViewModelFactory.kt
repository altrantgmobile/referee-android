package nl.altran.reversi.reversiwars.select_players

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import nl.altran.reversi.reversiwars.di.DaggerSelectPlayersComponent
import nl.altran.reversi.reversiwars.di.RemoteModule
import nl.altran.reversi.reversiwars.di.SelectPlayersModule

@Suppress("UNCHECKED_CAST")
class SelectPlayersViewModelFactory(private val fragment: SelectPlayersFragment) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DaggerSelectPlayersComponent
                .builder()
                .remoteModule(RemoteModule())
                .selectPlayersModule(SelectPlayersModule(fragment))
                .build()
                .viewmodel() as T
    }
}