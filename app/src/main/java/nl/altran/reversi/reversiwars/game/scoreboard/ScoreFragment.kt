package nl.altran.reversi.reversiwars.game.scoreboard

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import nl.altran.reversi.reversiwars.AnimationHelper
import nl.altran.reversi.reversiwars.R
import nl.altran.reversi.reversiwars.SlidableFragment
import nl.altran.reversi.reversiwars.databinding.FragmentScoreBinding
import nl.altran.reversi.reversiwars.game.ui.ReversiViewModel

class ScoreFragment : Fragment(), SlidableFragment {

    private lateinit var binding: FragmentScoreBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_score, container, false)
        binding.reversi = ViewModelProviders.of(activity!!).get(ReversiViewModel::class.java)
        return binding.root
    }

    override fun onSlide(perc: Float) {
        if (binding.winner.visibility == View.VISIBLE) {
            AnimationHelper.flyOut(binding.winner, 1F - perc)
            AnimationHelper.fadeIn(binding.fab, perc)
        } else {
            AnimationHelper.fadeIn(binding.fab, 1F)
        }
    }
}
