package nl.altran.reversi.reversiwars.select_players.usecases

import nl.altran.reversi.data.Move
import nl.altran.reversi.reversiwars.model.Player
import javax.inject.Inject


class StoreMoveUsecase @Inject constructor(private val repo: MatchRepository) {

    fun go(matchId: String, player: Player, move: Move) {
        repo.storeMove(matchId, MoveData(player, move))
    }
}