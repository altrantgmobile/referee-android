package nl.altran.reversi.reversiwars.model

data class Score(val white: Int, val black: Int, val empty: Int)