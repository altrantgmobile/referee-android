package nl.altran.reversi.reversiwars.game.usecases.firebase

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import nl.altran.reversi.data.Move
import nl.altran.reversi.data.ReversiResultData
import nl.altran.reversi.reversiwars.game.usecases.GameRepository
import nl.altran.reversi.reversiwars.model.Stone
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info


class FirebaseGameRepository(private val playerRef: DatabaseReference) : GameRepository {

    private lateinit var matchRef: DatabaseReference
    private var moveListener: ValueEventListener? = null

    companion object Scheme {
        private const val MATCHES = "matches"
        private const val STONECOLOR = "stoneColor"
        private const val STATS = "stats"
        private const val MOVE = "move"
        private const val BOARD = "board"
    }

    override fun joinGame(guid: String, color: Stone.Color) {
        this.matchRef = this.playerRef.child(MATCHES).child(guid)
        this.matchRef.child(STONECOLOR).setValue(color.asInt())
    }

    val logger = AnkoLogger<FirebaseGameRepository>()

    override fun gameFinished(yourScore: Int, opponentScore: Int, points: Int) {
        val statsRef = this.playerRef.child(STATS)
        val stats = ReversiResultData(yourScore, opponentScore, points + (yourScore - opponentScore))
        val statsWriter = FirebaseStatsWriter(stats)

        logger.info("gameFinished: $statsWriter, $stats")
        statsRef.addListenerForSingleValueEvent(statsWriter)
    }

    override fun handleTurn(board: String, done: (Move) -> Unit) {
        this.matchRef.child(BOARD).setValue(board)
        listenForResponse(done)
    }

    override fun reject() {
        this.matchRef.child(MOVE).setValue("rejected")
    }

    private fun listenForResponse(done: (Move) -> Unit) {
        this.moveListener = FirebaseMoveListener(done, { unlistenForResponse(it) })
        this.matchRef.child(MOVE).addValueEventListener(moveListener)
    }

    private fun unlistenForResponse(moveListener: FirebaseMoveListener) {
        this.moveListener = null
        this.matchRef.child(MOVE).removeEventListener(moveListener)
    }
}
