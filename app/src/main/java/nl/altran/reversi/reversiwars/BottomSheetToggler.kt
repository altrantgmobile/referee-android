package nl.altran.reversi.reversiwars

import android.support.design.widget.BottomSheetBehavior
import android.view.View

abstract class BottomSheetToggler : BottomSheetBehavior.BottomSheetCallback() {
    companion object {
        val SLIDING = -1
    }

    private var currentState = BottomSheetBehavior.STATE_SETTLING
    private var slide = 0F
    override final fun onStateChanged(view: View, state: Int) {
        if (shouldToggle(state)) {
            onToggleAndSlide(state, slide)
            currentState = state
        }
    }

    override final fun onSlide(view: View, v: Float) {
        slide = v
        onToggleAndSlide(SLIDING, slide)
    }

    private fun shouldToggle(state: Int)
            = currentState != state
            && (state == BottomSheetBehavior.STATE_EXPANDED
            || state == BottomSheetBehavior.STATE_COLLAPSED)

    abstract fun onToggleAndSlide(index: Int, perc: Float)
}