package nl.altran.reversi.reversiwars.game

import java.util.concurrent.TimeUnit

data class GameSettings(val timeout: Long = 8, val unit: TimeUnit = TimeUnit.SECONDS, val size: Int = 8)