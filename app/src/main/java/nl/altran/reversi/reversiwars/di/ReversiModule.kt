package nl.altran.reversi.reversiwars.di

import android.arch.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import nl.altran.reversi.reversiwars.FragmentSwapper
import nl.altran.reversi.reversiwars.ReversiActivity
import nl.altran.reversi.reversiwars.game.ui.ReversiViewModel

@Module
class ReversiModule(private val reversiActivity: ReversiActivity) {

    private val viewModel: ReversiViewModel = ViewModelProviders.of(reversiActivity).get(ReversiViewModel::class.java)
    private val swapper: FragmentSwapper = FragmentSwapper(reversiActivity.supportFragmentManager)

    @Provides
    fun provideViewModel(): ReversiViewModel {
        viewModel.playPauseCallback = reversiActivity
        return viewModel
    }

    @Provides
    fun provideSwapper(): FragmentSwapper {
        return swapper
    }
}