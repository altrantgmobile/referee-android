package nl.altran.reversi.reversiwars.select_players

import android.databinding.BindingAdapter
import android.support.design.widget.FloatingActionButton
import android.widget.TextView
import nl.altran.reversi.reversiwars.R
import nl.altran.reversi.reversiwars.model.Player

object SelectPlayersBinding {

    @JvmStatic
    @BindingAdapter("fabIconWinner")
    fun doSetFabIconForState(view: FloatingActionButton, winner: Player?) {
        if (winner != null) {
            // Grow smaller
            view.setImageResource(R.drawable.ic_refresh)
        } else {
            view.setImageResource(R.drawable.ic_play)
        }
    }

    @JvmStatic
    @BindingAdapter("stoneDrawable")
    fun showCurrentSelector(view: TextView, current: Int) {
        when (current) {
            0 -> view.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_stone_white, 0, R.drawable.ic_empty_stone, 0)
            1 -> view.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_empty_stone, 0, R.drawable.ic_stone_black, 0)
            else -> {
            }
        }
    }
}