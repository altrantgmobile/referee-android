package nl.altran.reversi.reversiwars

interface SlidableFragment {
    fun onSlide(perc: Float)
}