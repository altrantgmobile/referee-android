package nl.altran.reversi.reversiwars.di

import com.google.firebase.database.FirebaseDatabase
import dagger.Module
import dagger.Provides
import nl.altran.reversi.reversiwars.select_players.usecases.MatchRepository
import nl.altran.reversi.reversiwars.select_players.usecases.PlayersRepository
import nl.altran.reversi.reversiwars.select_players.usecases.firebase.FirebaseMatchRepository
import nl.altran.reversi.reversiwars.select_players.usecases.firebase.FirebasePlayersRepository

@Module
class RemoteModule {

    @Provides
    fun provideMatchRepository(): MatchRepository {
        return FirebaseMatchRepository()
    }

    @Provides
    fun providePlayersRepository(database: FirebaseDatabase) : PlayersRepository {
        return FirebasePlayersRepository(database)
    }

    @Provides
    fun provideRemoteLocation() : FirebaseDatabase {
        return FirebaseDatabase.getInstance()
    }
}