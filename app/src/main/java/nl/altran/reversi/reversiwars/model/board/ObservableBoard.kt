package nl.altran.reversi.reversiwars.model.board

import android.databinding.ListChangeRegistry
import android.databinding.ObservableList
import nl.altran.reversi.data.Move
import nl.altran.reversi.reversiwars.model.Stone

class ObservableBoard(private val board: Board) : ArrayList<Stone>(), ObservableList<Stone> {

    @Transient private var mListeners: ListChangeRegistry = ListChangeRegistry()

    init {
        board.stones.forEach {
            add(it)
        }
    }

    override fun addOnListChangedCallback(listener: ObservableList.OnListChangedCallback<out ObservableList<Stone>>?) {
        mListeners.add(listener)
    }

    override fun removeOnListChangedCallback(listener: ObservableList.OnListChangedCallback<out ObservableList<Stone>>?) {
        mListeners.remove(listener)
    }

    override fun add(element: Stone): Boolean {
        super.add(element)
        this.notifyAdd(this.size - 1, 1)
        return true
    }

    override fun add(index: Int, element: Stone) {
        super.add(index, element)
        this.notifyAdd(index, 1)
    }

    override fun addAll(elements: Collection<Stone>): Boolean {
        val oldSize = this.size
        val added = super.addAll(elements)
        if (added) {
            this.notifyAdd(oldSize, this.size - oldSize)
        }

        return added
    }

    override fun addAll(index: Int, elements: Collection<Stone>): Boolean {
        val added = super.addAll(index, elements)
        if (added) {
            this.notifyAdd(index, elements.size)
        }

        return added
    }

    override fun clear() {
        val oldSize = this.size
        super.clear()
        if (oldSize != 0) {
            this.notifyRemove(0, oldSize)
        }
    }

    override fun removeAt(index: Int): Stone {
        val value = super.removeAt(index)
        this.notifyRemove(index, 1)
        return value
    }

    override fun remove(element: Stone): Boolean {
        val index = this.indexOf(element)
        return if (index >= 0) {
            this.removeAt(index)
            true
        } else {
            false
        }
    }

    override fun set(index: Int, element: Stone): Stone {
        val value = super.set(index, element)
        this.mListeners.notifyChanged(this, index, 1)

        return value
    }

    override fun removeRange(fromIndex: Int, toIndex: Int) {
        super.removeRange(fromIndex, toIndex)
        this.notifyRemove(fromIndex, toIndex - fromIndex)
    }

    private fun notifyAdd(start: Int, count: Int) {
        this.mListeners.notifyInserted(this, start, count)
    }

    private fun notifyRemove(start: Int, count: Int) {
        this.mListeners.notifyRemoved(this, start, count)
    }

    fun restart() {
        clear()
        board.restart()
        board.stones.forEach {
            add(it)
        }
    }

    fun performMove(move: Move, stoneColor: Stone.Color, list: List<Stone>) {
        board.stones.forEach {
            it.last(false)
        }

        list.forEach {
            board.get(it.row, it.col).flip()
        }

        board.get(move.row, move.col).last(true).set(stoneColor)
    }

    override fun toString(): String {
        return board.toString()
    }
}