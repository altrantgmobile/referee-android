package nl.altran.reversi.reversiwars.di

import dagger.Module
import dagger.Provides
import nl.altran.reversi.reversiwars.select_players.SelectPlayersFragment
import nl.altran.reversi.reversiwars.select_players.SelectPlayersViewModel
import nl.altran.reversi.reversiwars.select_players.usecases.CreateMatchUsecase
import nl.altran.reversi.reversiwars.select_players.usecases.FetchPlayersUsecase
import nl.altran.reversi.reversiwars.select_players.usecases.MatchRepository
import nl.altran.reversi.reversiwars.select_players.usecases.PlayersRepository

@Module
class SelectPlayersModule(private val fragment: SelectPlayersFragment) {

    @Provides
    fun provideSelectPlayersViewModel(fetchPlayersUsecase: FetchPlayersUsecase, createMatchUsecase: CreateMatchUsecase): SelectPlayersViewModel {
        return SelectPlayersViewModel(fragment.lifecycle, fetchPlayersUsecase, createMatchUsecase)
    }

    @Provides
    fun provideCreateMatchUsecase(matchRepository: MatchRepository): CreateMatchUsecase {
        return CreateMatchUsecase(fragment.activity as CreateMatchUsecase.Callback, matchRepository)
    }

    @Provides
    fun provideFetchPlayersUsecase(playersRepository: PlayersRepository): FetchPlayersUsecase {
        return FetchPlayersUsecase(playersRepository)
    }
}