package nl.altran.reversi.reversiwars.select_players.usecases.firebase

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import nl.altran.reversi.reversiwars.game.usecases.MatchNode
import nl.altran.reversi.reversiwars.model.Player
import nl.altran.reversi.reversiwars.select_players.usecases.MatchRepository
import nl.altran.reversi.reversiwars.select_players.usecases.MoveData

class FirebaseMatchRepository(private val dbRef: DatabaseReference = FirebaseDatabase.getInstance().getReference("matches")) : MatchRepository {

    override fun createMatch(matchNode: MatchNode,
                             onSuccess: (String, Player, Player) -> Unit,
                             onFail: (String) -> Unit) {
        val matchRef = dbRef.push()
        matchRef.setValue(matchNode, {
            databaseError, _ ->
            if (databaseError == null) onSuccess(matchRef.key, matchNode.p1, matchNode.p2)
            else onFail(databaseError.message)
        })
    }

    override fun storeMove(matchId: String, moveData: MoveData) {
        val ref = dbRef.child(matchId).child("_moves").push()
        ref.setValue("${moveData.player.color()}:${moveData.move.row},${moveData.move.col}")
    }
}