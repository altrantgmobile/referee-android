package nl.altran.reversi.reversiwars.select_players.usecases

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import nl.altran.reversi.reversiwars.model.Player

class PingUsecase(private var playerRef: DatabaseReference) : ValueEventListener {

    private lateinit var function: (Player) -> Unit
    private lateinit var player: Player
    private lateinit var pingReference: DatabaseReference
    private val ping = "Dude, you there?"

    fun ping(player: Player, function: (Player) -> Unit) {
        this.player = player
        this.function = function
        this.pingReference = playerRef.child(player.guid()).child("public").child("ping")
        this.pingReference.setValue(ping)
        this.pingReference.addValueEventListener(this)
    }

    override fun onCancelled(p0: DatabaseError?) {}

    override fun onDataChange(p0: DataSnapshot?) {
        if(p0?.value != ping && p0?.ref == pingReference){
            function(player)
            pingReference.removeEventListener(this)
        }
    }
}