package nl.altran.reversi.reversiwars.game.usecases

import nl.altran.reversi.data.Move
import nl.altran.reversi.reversiwars.model.Stone

interface GameRepository {
    fun joinGame(guid: String, color: Stone.Color)
    fun gameFinished(yourScore: Int, opponentScore: Int, points: Int)
    fun handleTurn(board: String, done: (Move) -> Unit)
    fun reject()
}