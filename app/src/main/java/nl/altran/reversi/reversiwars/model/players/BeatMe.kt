package nl.altran.reversi.reversiwars.model.players

import nl.altran.reversi.data.Move
import nl.altran.reversi.reversiwars.model.board.Board
import nl.altran.reversi.reversiwars.model.util.BoardUtil
import kotlin.math.pow


class BeatMe(val maxDepth: Int) : BasePlayer() {

    override fun name(): String {
        return when(maxDepth){
            5 -> "BeatMe++"
            else -> "BeatMe"
        }
    }

    override fun human(): Boolean {
        return false
    }

    override fun local(): Boolean {
        return true
    }

    override fun onRejected(board: String) {
        onYourTurn(board)
    }

    override fun onYourTurn(board: String) {
        val gameBoard = BoardUtil.extractBoard(board)
        val moveList = BoardUtil.findPossibleMoves(gameBoard, stoneColor)
        val miniMax = minimax(gameBoard, moveList, maxDepth, Integer.MIN_VALUE, Integer.MAX_VALUE, stoneColor.value, moveList[0])
        submitMove(miniMax.first)
    }

    private fun minimax(board: Board, moves: List<Move>, depth: Int, alpha: Int, beta: Int, color: Int, move: Move): Pair<Move, Int> {
        var bestMove = move
        var newAlpha = alpha
        var newBeta = beta

        if (depth == 0 || moves.isEmpty()) {
            return Pair(bestMove, eval(board, color))
        }

        if (color == stoneColor.value) {
            // MAX
            var score = Integer.MIN_VALUE
            for (it in moves) {
                val child = board.clone()
                child.applyMove(it, color)
                val maxi = minimax(child, BoardUtil.findPossibleMoves(child, stoneColor.flip()), depth - 1, newAlpha, newBeta, -1 * color, it)
                if (maxi.second > score) {
                    bestMove = it
                    score = Math.max(score, maxi.second)
                }
                newAlpha = Math.max(alpha, score)
                if (newBeta <= newAlpha) break
            }

            return Pair(bestMove, score)

        } else {
            // MIN
            var score = Integer.MAX_VALUE
            for (it in moves) {
                val child = board.clone().apply { applyMove(it, color) }
                val flippedColor = -1 * color
                val mini = minimax(child, BoardUtil.findPossibleMoves(child, flippedColor), depth - 1, newAlpha, newBeta, flippedColor, it)
                if (mini.second < score) {
                    bestMove = it
                    score = Math.min(score, mini.second)
                }
                newBeta = Math.min(newBeta, score)
                if (newBeta <= newAlpha) break
            }

            return Pair(bestMove, score)
        }
    }

    private fun eval(board: Board, color: Int): Int {
        val corners = 100 * evalCorners(board)
        val stones = evalScore(board)
        val opponentMoves = BoardUtil.findPossibleMoves(board, -1 * color).size.toDouble().pow(2).toInt()
        val eval = corners + stones - opponentMoves
        return stoneColor.value * eval
    }

    private fun evalCorners(board: Board): Int {
        val size = board.size - 1
        return arrayOf(0, size, size * size + 1, size * size + size).sumBy { board.get(it).color.get()!!.value }
    }

    private fun evalScore(board: Board): Int {
        return board.stones.sumBy { board.get(it.row, it.col).color.get()!!.value }
    }
}