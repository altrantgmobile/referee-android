package nl.altran.reversi.data

import com.google.gson.annotations.SerializedName

data class BoardData(@SerializedName("size") val size: Int, @SerializedName("board") val raw: Array<IntArray>) {

    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }

    override fun toString(): String {
        return super.toString()
    }

    fun filterNonEmpty(possibleMoves: MutableList<Move>) : MutableList<Move> {
        return possibleMoves.filter {
            raw[it.row][it.col] == 0
        }.toMutableList()
    }
}