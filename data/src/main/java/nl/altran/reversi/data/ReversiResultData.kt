package nl.altran.reversi.data

data class ReversiResultData(private val yourScore: Int, private val opponentScore: Int, val points: Int) {
    val win: Int = if(yourScore > opponentScore) 1 else 0
    val lost: Int = if(yourScore < opponentScore) 1 else 0
    val drew: Int = if(yourScore == opponentScore) 1 else 0
}