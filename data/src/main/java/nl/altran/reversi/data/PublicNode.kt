package nl.altran.reversi.data

import com.google.gson.annotations.SerializedName

class PublicNode constructor(){

    constructor( name: String, ping: String) : this(){
        val publicData = PublicData()
        publicData.name = name
        publicData.ping = ping
        public = publicData
    }

    @SerializedName("public") lateinit var public: PublicData

    class PublicData{
        @SerializedName("name") lateinit var name: String
        @SerializedName("ping") lateinit var ping: String
    }
}