package nl.altran.reversi.data

import com.google.gson.annotations.SerializedName

data class Move constructor(@SerializedName("row") val row: Int,
                            @SerializedName("col") val col: Int)