package nl.altran.reversi.data

import com.google.gson.annotations.SerializedName

data class PlayerNode(

        @SerializedName("matches") val matches: Map<String, Match> = emptyMap(),
        @SerializedName("stats") val stats: Stats = Stats(0, 0, 0, 0)) {

    data class Stats(
            @SerializedName("score") val score: Int,
            @SerializedName("win") val win: Int,
            @SerializedName("draw") val draw: Int,
            @SerializedName("lost") val lost: Int) {
        fun update(result: ReversiResultData): Stats {
            return copy(score = score + result.points,
                    win = win + result.win,
                    draw = draw + result.drew,
                    lost = lost + result.lost)
        }
    }

    data class Match(@SerializedName("board") val board: BoardData,
                     @SerializedName("move") val move: Move,
                     @SerializedName("stoneColor") val stoneColor: String)

}