package nl.altran.reversi.sample

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import nl.altran.reversi.bot.FirebaseBot
import nl.altran.reversi.data.BoardData
import nl.altran.reversi.data.Move

class RandomBot(email: String, name: String) : FirebaseBot(email, name) {

    private val gson: Gson by lazy { GsonBuilder().create() }
    private var boardData: BoardData = BoardData(0, arrayOf(intArrayOf()))
    private lateinit var boardString : String
    private var color: Int = -1
    private var possibleMoves = emptyList<Move>().toMutableList()

    override fun onJoinedGame(guid: String, color: Int) {
        this.color = color
    }

    override fun onYourTurn(board: String) {
        boardString = board
        boardData = gson.fromJson(board, BoardData::class.java)

        if (possibleMoves.isEmpty()) {

            possibleMoves.add(Move(4, 4))
            possibleMoves.add(Move(3, 4))
            possibleMoves.add(Move(4, 3))
            possibleMoves.add(Move(3, 3))

            possibleMoves = boardData.findMovesFor(color)
        }

        possibleMoves = boardData.filterNonEmpty(possibleMoves)

        if (possibleMoves.isNotEmpty()) {
            val row = possibleMoves.removeAt(0).row
            val col = possibleMoves.removeAt(0).col
            submit("{\"row\":$row, \"col\":$col}")
        }
    }

    override fun onRejected(board: String) {
        // Try again
        onYourTurn(boardString)
    }

    override fun onGameFinished(yourScore: Int, opponentScore: Int) {}

    fun BoardData.findMovesFor(color: Int): MutableList<Move> {
        val moves = emptyList<Move>().toMutableList()
        for (row in 0 until raw.size) {
            for (col in 0 until raw[row].size) {
                if (raw[row][col] == 0) {
                    moves.add(Move(row, col))
                }
            }

        }
        return moves
    }

    private fun isValid(raw: Array<IntArray>, color: Int, move: Move): Boolean {
        return true // raw[move.row][move.col] = -1 * color ||
    }
}