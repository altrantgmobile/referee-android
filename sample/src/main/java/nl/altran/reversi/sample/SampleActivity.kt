package nl.altran.reversi.sample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import nl.altran.reversi.bot.Bot

class SampleActivity : AppCompatActivity() {

    lateinit var bot1 : Bot
    lateinit var bot2 : Bot

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sample)

//        bot1 = SampleBot("entreco@gmail.com", "Tamenori")
        bot2 = RandomBot("a.b@c.com", "Okagachi")
    }

    override fun onDestroy() {
        bot1.kill()
        bot2.kill()
        super.onDestroy()
    }
}
