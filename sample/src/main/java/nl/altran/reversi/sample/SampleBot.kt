package nl.altran.reversi.sample

import android.util.Log
import nl.altran.reversi.bot.FirebaseBot

class SampleBot(email: String, bot: String) : FirebaseBot(email, bot) {
    override fun onJoinedGame(guid: String, color: Int) {}

    override fun onYourTurn(board: String) {
        val row : Int = (Math.random() * 8).toInt()
        val col : Int = (Math.random() * 8).toInt()
        submit("{\"row\":$row, \"col\":$col}")
    }

    override fun onRejected(board: String) {
        onYourTurn(board)
    }

    override fun onGameFinished(yourScore: Int, opponentScore: Int) {
        Log.i("Ox", "yeah")
    }
}