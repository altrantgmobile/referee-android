package nl.altran.reversi.bot

interface Bot {
    fun onJoinedGame(guid: String, color: Int)
    fun onYourTurn(board: String)
    fun onRejected(board: String)
    fun onGameFinished(yourScore: Int, opponentScore: Int)
    fun kill()
}