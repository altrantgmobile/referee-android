package nl.altran.reversi.bot

import android.support.annotation.CallSuper
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

abstract class FirebaseBot(private val email: String,
                           private val botname: String,
                           private val pingUsecase: PongUsecase = PongUsecase(email),
                           private val joinUsecase: JoinUsecase = JoinUsecase(FirebaseRemoteRepository()),
                           private val submitUsecase: SubmitUsecase = SubmitUsecase(),
                           private val registerUsecase: RegisterUsecase = RegisterUsecase(),
                           private val turnUsecase: TurnUsecase = TurnUsecase()) : Bot {

    private val logger = AnkoLogger<FirebaseBot>()

    init {
        register()
    }

    private fun register() {
        logger.info("register")
        registerUsecase.register(botname, "pong", {
            logger.info("register done key($it)")
            pingUsecase.register(it)
            joinUsecase.register(it, { matchId: String, color: Int ->
                logger.info("join done matchId($matchId) color($color)")
                joinUsecase.unregister()
                onJoinedGame(matchId, color)
                submitUsecase.register(it, matchId, {
                    logger.info("rejected turn")
                    onRejected("niy")
                })
                turnUsecase.register(it, matchId, {
                    logger.info("your turn board($it)")
                    onYourTurn(it)
                })
            })
        })
    }


    @CallSuper
    override fun kill() {
        logger.info("kill()")
        registerUsecase.unregister()
        pingUsecase.unregister()
        joinUsecase.unregister()
        turnUsecase.unregister()
    }

    protected fun submit(s: String) {
        submitUsecase.submit(s)
    }
}