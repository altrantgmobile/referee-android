package nl.altran.reversi.bot

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import org.jetbrains.anko.AnkoLogger

class FirebaseRemoteRepository : RemoteRepository {

    private lateinit var initReference: DatabaseReference
    private lateinit var readyListener: ReadyEventListener
    private lateinit var joinListener: JoinEventListener
    private lateinit var addListener: AddEventListener

    val logger = AnkoLogger<FirebaseRemoteRepository>()

    override fun register(key: String, join: (String, Int) -> Unit) {
        this.initReference = FirebaseDatabase.getInstance().getReference("players").child(key).child("matches")

        this.addListener = AddEventListener()
        this.joinListener = JoinEventListener(join)
        this.readyListener = ReadyEventListener {
            unregister()
            initReference.addChildEventListener(joinListener)
        }

        this.initReference.addListenerForSingleValueEvent(readyListener)
        this.initReference.addChildEventListener(addListener)
    }

    override fun unregister() {
        initReference.removeEventListener(readyListener)
        initReference.removeEventListener(joinListener)
        initReference.removeEventListener(addListener)
    }
}