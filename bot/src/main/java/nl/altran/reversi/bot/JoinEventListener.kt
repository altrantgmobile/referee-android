package nl.altran.reversi.bot

import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class JoinEventListener(private val done: (String, Int) -> Unit) : ChildEventListener {

    val logger = AnkoLogger<FirebaseRemoteRepository>()

    override fun onChildMoved(p0: DataSnapshot?, p1: String?) {}

    override fun onCancelled(p0: DatabaseError?) {}

    override fun onChildChanged(p0: DataSnapshot?, p1: String?) {}

    override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
        logger.info("onChildAdded -> we have joined a match")
        done(p0!!.ref!!.key, p0.child("stoneColor").hashCode())
    }

    override fun onChildRemoved(p0: DataSnapshot?) {}
}