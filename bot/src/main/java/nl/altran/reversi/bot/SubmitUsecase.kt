package nl.altran.reversi.bot

import com.google.firebase.database.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

open class SubmitUsecase : ValueEventListener {

    lateinit var submitReference : DatabaseReference
    lateinit var rejecter: () -> Unit

    fun register(playerId: String, matchId: String, rejecter: () -> Unit) {
        this.rejecter = rejecter
        this.submitReference = FirebaseDatabase.getInstance().getReference("players").child(playerId).child("matches").child(matchId).child("move")
        this.submitReference.addValueEventListener(this)
    }

    fun submit(json: String){
        this.submitReference.setValue(json)
    }

    private val logger = AnkoLogger<SubmitUsecase>()
    override fun onDataChange(p0: DataSnapshot?) {
        logger.info("submit ${submitReference}")
        logger.info("submit onDataChange $p0, ${p0?.value}")
        if(p0?.value.toString() == "rejected"){
            rejecter.invoke()
        }
    }

    override fun onCancelled(p0: DatabaseError?) {}
}