package nl.altran.reversi.bot

open class JoinUsecase(private val repo: RemoteRepository) {

    fun register(key: String, join: (String, Int) -> Unit) {
        repo.register(key, join)
    }

    fun unregister(){
        repo.unregister()
    }
}