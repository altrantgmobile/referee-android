package nl.altran.reversi.bot

import com.google.firebase.database.*

open class PongUsecase(private val pong: String) : ValueEventListener {

    private lateinit var pongReference: DatabaseReference

    fun register(key: String) {
        pongReference = FirebaseDatabase.getInstance().getReference("players").child(key).child("public").child("ping")
        pongReference.addValueEventListener(this)
    }

    fun unregister() {
        pongReference.removeEventListener(this)
    }

    override fun onDataChange(p0: DataSnapshot?) {
        if(p0?.value != pong) {
            pongReference.setValue(pong)
        }
    }

    override fun onCancelled(p0: DatabaseError?) {

    }
}