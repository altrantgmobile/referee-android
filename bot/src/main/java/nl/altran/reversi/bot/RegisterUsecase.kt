package nl.altran.reversi.bot

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import nl.altran.reversi.data.PublicNode

open class RegisterUsecase : ExistingPlayersListener.Callback {

    private var callback: (String) -> Unit = {}

    override fun existingPlayer(botname: String, reply: String, ref: DatabaseReference) {
        this.callback.invoke(ref.key)
    }

    override fun newPlayer(botname: String, reply: String, ref: DatabaseReference) {
        ref.setValue(PublicNode(botname, reply))
        this.callback.invoke(ref.key)
    }

    private val playersReference by lazy {
        FirebaseDatabase.getInstance().getReference("players")
    }

    fun register(botname: String, reply: String, callback: (String) -> Unit) {
        this.callback = callback
        val listener = ExistingPlayersListener(botname, reply, this)
        playersReference.addListenerForSingleValueEvent(listener)
    }

    fun unregister() {}
}