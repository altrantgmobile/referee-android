package nl.altran.reversi.bot

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

open class TurnUsecase {

    private lateinit var turnReference: DatabaseReference
    private lateinit var turnListener: TurnListener
    private val logger = AnkoLogger<TurnUsecase>()

    fun register(playerId: String, matchId: String, turner: (String) -> Unit) {
        this.turnListener = TurnListener(turner)
        this.turnReference = FirebaseDatabase.getInstance().getReference("players").child(playerId).child("matches").child(matchId).child("board")
        logger.info("turn ${turnReference}")
        this.turnReference.addValueEventListener(turnListener)
    }

    fun unregister(){
        this.turnReference.removeEventListener(turnListener)
    }
}