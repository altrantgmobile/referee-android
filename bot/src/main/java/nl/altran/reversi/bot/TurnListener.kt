package nl.altran.reversi.bot

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class TurnListener(private var turner: (String) -> Unit) : ValueEventListener {
    override fun onCancelled(p0: DatabaseError?) {
    }

    private val logger = AnkoLogger<TurnListener>()
    override fun onDataChange(p0: DataSnapshot?) {
        logger.info("turn onDataChange $p0, ${p0?.value}")
        if(p0 != null && p0.value != null) {
            this.turner.invoke(p0.getValue(String::class.java)!!)
        }
    }
}