package nl.altran.reversi.bot

interface RemoteRepository {
    fun register(key: String, join: (String, Int) -> Unit)
    fun unregister()
}