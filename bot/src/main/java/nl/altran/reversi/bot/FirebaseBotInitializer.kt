package nl.altran.reversi.bot

import android.content.ContentProvider
import android.content.ContentValues
import android.database.Cursor
import android.net.Uri
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions


class FirebaseBotInitializer : ContentProvider() {


    override fun onCreate(): Boolean {
        val options = FirebaseOptions.Builder()
                .setApiKey("AIzaSyA7ruSb75KcdYojOy4UDX-JNC_vagp01no")
                .setApplicationId("nl.altran.reversi.reversiwars")
                .setDatabaseUrl("https://reversi-wars.firebaseio.com")
                .setProjectId("reversi-wars")
                .build()

        FirebaseApp.initializeApp(context, options)
        return true
    }

    override fun insert(p0: Uri?, p1: ContentValues?): Uri {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun query(p0: Uri?, p1: Array<out String>?, p2: String?, p3: Array<out String>?, p4: String?): Cursor {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun update(p0: Uri?, p1: ContentValues?, p2: String?, p3: Array<out String>?): Int {
        return 0
    }

    override fun delete(p0: Uri?, p1: String?, p2: Array<out String>?): Int {
        return 0
    }

    override fun getType(p0: Uri?): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}