package nl.altran.reversi.bot

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

class ReadyEventListener(private val onReady: () -> Unit) : ValueEventListener {
    override fun onCancelled(p0: DatabaseError?) {}

    override fun onDataChange(p0: DataSnapshot?) {
        onReady()
    }
}