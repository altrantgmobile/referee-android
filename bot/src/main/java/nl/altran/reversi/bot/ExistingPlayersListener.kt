package nl.altran.reversi.bot

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener

class ExistingPlayersListener(val botname: String, val reply: String, val callback: Callback) : ValueEventListener {

    interface Callback{
        fun existingPlayer(botname: String, reply: String, ref: DatabaseReference)
        fun newPlayer(botname:String, reply:String, ref: DatabaseReference)
    }

    override fun onCancelled(p0: DatabaseError?) {}

    override fun onDataChange(p0: DataSnapshot?) {
        var existingPlayer = false
        loop@ for(ref in p0?.children?.iterator()!!){
            if(checkExists(ref, botname)){
                existingPlayer = true
                callback.existingPlayer(botname, reply, ref.ref)
                break@loop
            }
        }

        if(!existingPlayer) {
            callback.newPlayer(botname, reply, p0.ref.push())
        }
    }

    private fun checkExists(snapshot: DataSnapshot?, botname: String) : Boolean {
        val shot = snapshot?.value.toString()
        return shot.contains("name=$botname")
    }
}