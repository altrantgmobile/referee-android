package nl.altran.reversi.bot

import com.nhaarman.mockito_kotlin.mock
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class FirebaseBotTest {

    val mockPingUsecase = mock<PingUsecase>()
    val mockJoinUsecase = mock<JoinUsecase>()
    val mockSubmitUsecase = mock<SubmitUsecase>()
    val mockRegisterUsecase = mock<RegisterUsecase>()
    val mockTurnUsecase = mock<TurnUsecase>()
    lateinit var subject: FirebaseBot

    @Test
    fun `it should register bot with firebase`() {
        Assert.assertTrue(true)
    }
}
