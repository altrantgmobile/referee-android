package nl.altran.reversi.bot

class MockBot(
        pingUsecase: PingUsecase,
        joinUsecase: JoinUsecase,
        submitUsecase: SubmitUsecase,
        registerUsecase: RegisterUsecase,
        turnUsecase: TurnUsecase

) : FirebaseBot("a@b.com", "bot", pingUsecase, joinUsecase, submitUsecase, registerUsecase, turnUsecase) {
    override fun onJoinedGame(guid: String, color: Int) {}

    override fun onYourTurn(board: String) {}

    override fun onRejected(board: String) {}

    override fun onGameFinished(yourScore: Int, opponentScore: Int) {}
}